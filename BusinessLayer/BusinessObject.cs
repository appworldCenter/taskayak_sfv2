﻿using DataBaseLayer.DBService;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class BusinessObject
    {

        #region Account Services
        public int validateLogin(LoginModel _login, ref string message, ref int roleid, ref bool isresetneeded, ref bool _status, ref bool accepttermsrqud, ref int workspaceid)
        {
            accountservices _accountService = new accountservices();
            return _accountService.validateLogin(_login, ref message, ref roleid, ref isresetneeded, ref _status, ref accepttermsrqud, ref workspaceid);
        }
        public string UpdateBilling(billingModal _model, int workspaceid)
        {
            accountservices _accountService = new accountservices();
            return _accountService.UpdateBilling(_model, workspaceid);
        }
        public string Updatepacjkage(int workspaceid, int smsCount, int emailcount, string subscriptionId, string CustomerId, string PlanName)
        {
            accountservices _accountService = new accountservices();
            return _accountService.Updatepacjkage(workspaceid, smsCount, emailcount, subscriptionId, CustomerId, PlanName);
        }
        public string UpdateBillingwithupgrade(string accounttype, int workspaceid, int email, int sms, string customerid, string subscriptionid, int planid, int userid)
        {
            accountservices _accountService = new accountservices();
            return _accountService.UpdateBillingwithupgrade(accounttype, workspaceid, email, sms, customerid, subscriptionid, planid, userid);
        }
        public string deactivateaccount(int workspaceid)
        {
            accountservices _accountService = new accountservices();
            return _accountService.deactivateaccount(workspaceid);
        }
        public string getaccounttype(int workspaceid)
        {
            accountservices _accountService = new accountservices();
            return _accountService.getaccounttype(workspaceid);
        }
        public billingModal getBilling(int workspaceid)
        {
            accountservices _accountService = new accountservices();
            return _accountService.getBilling(workspaceid);  
        }
        public bool validate_email(string email, int workspaceid)
        {
            accountservices _accountService = new accountservices();
            return _accountService.validate_email(email,workspaceid); 
        } 
        public bool validate_memberid(string memberid, int workspaceid)
        {
            accountservices _accountService = new accountservices();
            return _accountService.validate_memberid(memberid, workspaceid);
        } 
        public bool validate_username(string username, int workspaceid)
        {
            accountservices _accountService = new accountservices();
            return _accountService.validate_username(username, workspaceid);
        }
        public string reset_password(string username, string tempasswors, ref string name, ref string email)
        {
            accountservices _accountService = new accountservices();
            return _accountService.reset_password(username, tempasswors, ref name, ref email);
        }
        public string update_password(int memberid, string password)
        {
            accountservices _accountService = new accountservices();
            return _accountService.update_password(memberid, password);
        }
        public string validatepassword(string c_password, string n_password, int memberid)
        {
            accountservices _accountService = new accountservices();
            return _accountService.validatepassword(c_password, n_password, memberid);
        }
            #endregion







        }
}
