﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace DataBaseLayer.Helper
{/// <summary>
 /// Static helper
 /// </summary>
    public static class ExtrensionHelper
    {
        /// <summary>
        ///Get User Status by User ID
        /// </summary>
        /// <param name="value">UserId</param>
        /// <returns>User Status</returns>
        public static string GetUserStatusById(this int _statusId)
        {
            string _status;
            switch (_statusId)
            {
                case 2:
                    _status = "Inactive";
                    break;
                default:
                    _status = "Active";
                    break;
            }
            return _status;
        }
        /// <summary>
        /// Format string into USA phone number format
        /// </summary>
        /// <param name="value">String </param>
        /// <returns>Formatted string</returns>
        public static string PhoneNumber(this string _phoneNumber)
        {
            string oldvalue = _phoneNumber;
            if (!string.IsNullOrEmpty(_phoneNumber))
            {
                _phoneNumber = _phoneNumber.Replace("-", "");
                _phoneNumber = _phoneNumber.Replace("+", "");
                _phoneNumber = _phoneNumber.Replace(".", "");
                _phoneNumber = _phoneNumber.Replace("(", "");
                _phoneNumber = _phoneNumber.Replace(")", "");
                _phoneNumber = new System.Text.RegularExpressions.Regex(@"\D").Replace(_phoneNumber, string.Empty);
                if (string.IsNullOrEmpty(_phoneNumber))
                {
                    _phoneNumber = oldvalue;
                }
                else
                {
                    if (_phoneNumber.Length > 10)
                    {
                        _phoneNumber = _phoneNumber.TrimStart('1');
                    }
                    if (_phoneNumber.Length < 10)
                    {
                        _phoneNumber = "###-###-####";
                    }
                    else
                    {
                        if (_phoneNumber.Length == 7)
                            return Convert.ToInt64(_phoneNumber).ToString("###-####");
                        if (_phoneNumber.Length == 10)
                            return Convert.ToInt64(_phoneNumber).ToString("###-###-####");
                        if (_phoneNumber.Length > 10)
                            return Convert.ToInt64(_phoneNumber).ToString("###-###-#### " + new String('#', (_phoneNumber.Length - 10)));

                    }
                }
            }
            else
            {
                _phoneNumber = "###-###-####";
            }
            return _phoneNumber;
        }
        /// <summary>
        /// Convert string in USA Currency format
        /// </summary>
        /// <param name="_amount">string</param>
        /// <returns>Formatted Currency string</returns>
        public static string Currency(this string _amount)
        {
            if (!string.IsNullOrEmpty(_amount))
            {
                _amount = _amount.Replace("$", "");
                try
                {
                    var _amountToConvert = Convert.ToDouble(_amount);
                    _amount = String.Format(new CultureInfo("en-US"), "{0:C}", _amountToConvert);
                }
                catch
                {
                    _amount = "$0.00";
                }
            }
            else
            {
                _amount = "$0.00";
            }
            return _amount;
        }
        /// <summary>
        /// Convert first letter as upper case letter
        /// </summary>
        /// <param name="_stringToFormat">string</param>
        /// <returns>Formated string with first upper case letter</returns>
        public static string ToTitleCase(this string _stringToFormat)
        {
            string _result = _stringToFormat;
            if (!string.IsNullOrEmpty(_stringToFormat))
            {
                var _words = _stringToFormat.Split(' ');
                for (int index = 0; index < _words.Length; index++)
                {
                    var s = _words[index];
                    if (s.Length > 0)
                    {
                        _words[index] = s[0].ToString().ToUpper() + s.Substring(1);
                    }
                }
                _result = string.Join(" ", _words);
            }
            return _result;
        }
        /// <summary>
        /// Get address part from a Google formatted address
        /// </summary>
        /// <param name="_formattedAddress">Street city, State zipcode</param>
        /// <param name="_street">Ref Street</param>
        /// <param name="_city">Ref City</param>
        /// <param name="_state">Ref State</param>
        /// <param name="_zipcode">Ref Zipcode</param>
        public static void GetAddress(this string _formattedAddress, ref string _street, ref string _city, ref string _state, ref string _zipcode)
        {
            var _address = _formattedAddress.Split(',');
            _street = _address[0];
            _city = _address[1].Trim();
            var _adddress1 = _address[2].TrimStart().Split(' ');
            _state = _adddress1[0];
            _zipcode = _adddress1[1];
        }
        /// <summary>
        /// Create address for show on detail page 
        /// </summary>
        /// <param name="_formattedAddress">Street city, State zipcode</param>
        /// <param name="_suiteNumber">Suite Number</param>
        /// <returns></returns>
        public static string GetAddressView(this string _formattedAddress, string _suiteNumber = "")
        {
            string _viewAddress;
            string _street = "";
            string _city = "";
            string _state = "";
            string _zipcode = "";
            if (!string.IsNullOrEmpty(_formattedAddress))
            {
                var _address = _formattedAddress.Split(',');
                _street = _address[0];
                _city = _address[1].Trim();
                var adddress1 = _address[2].TrimStart().Split(' ');
                _state = adddress1[0];
                _zipcode = adddress1[1];
            }
            _viewAddress = _street;
            if (!string.IsNullOrEmpty(_suiteNumber))
            {
                if (!string.IsNullOrEmpty(_street))
                {
                    _viewAddress = _viewAddress + " <br/>" + _suiteNumber;
                }
                else
                {
                    _viewAddress = _suiteNumber;
                }
            }
            if (!string.IsNullOrEmpty(_city))
            {
                if (!string.IsNullOrEmpty(_viewAddress))
                {
                    _viewAddress = _viewAddress + " <br/>" + _city;
                }
                else
                {
                    _viewAddress = _city;
                }
            }
            if (!string.IsNullOrEmpty(_state))
            {
                if (!string.IsNullOrEmpty(_viewAddress))
                {
                    _viewAddress = _viewAddress + ", " + _state.ToUpper();
                }
                else
                {
                    _viewAddress = _state.ToUpper();
                }
            }
            if (!string.IsNullOrEmpty(_zipcode))
            {
                if (!string.IsNullOrEmpty(_viewAddress))
                {
                    _viewAddress = _viewAddress + "  " + _zipcode;
                }
                else
                {
                    _viewAddress = _zipcode;
                }

            }
            return _viewAddress;
        }
        /// <summary>
        /// Create formated address for details page using differnt data
        /// </summary>
        /// <param name="_street"></param>
        /// <param name="_city"></param>
        /// <param name="_state"></param>
        /// <param name="_zipcode"></param>
        /// <param name="_suiteNumber"></param>
        /// <returns></returns>
        public static string GetAddress(this string _street, string _city, string _state, string _zipcode, string _suiteNumber)
        {
            string _formattedAddress = "";
            if (!string.IsNullOrEmpty(_street))
            {
                _formattedAddress = _street;
            }

            if (!string.IsNullOrEmpty(_suiteNumber))
            {
                if (!string.IsNullOrEmpty(_formattedAddress))
                {
                    _formattedAddress = _formattedAddress + " <br/>" + _suiteNumber;
                }
                else
                {
                    _formattedAddress = _suiteNumber;
                }

            }
            if (!string.IsNullOrEmpty(_city))
            {
                if (!string.IsNullOrEmpty(_formattedAddress))
                {
                    _formattedAddress = _formattedAddress + " <br/>" + _city;
                }
                else
                {
                    _formattedAddress = _city;
                }

            }
            if (!string.IsNullOrEmpty(_state))
            {
                if (!string.IsNullOrEmpty(_formattedAddress))
                {
                    _formattedAddress = _formattedAddress + ", " + _state.ToUpper();
                }
                else
                {
                    _formattedAddress = _state.ToUpper();
                }

            }
            if (!string.IsNullOrEmpty(_zipcode))
            {
                if (!string.IsNullOrEmpty(_formattedAddress))
                {
                    _formattedAddress = _formattedAddress + "  " + _zipcode;
                }
                else
                {
                    _formattedAddress = _zipcode;
                }
            }
            return _formattedAddress;
        }
        /// <summary>
        /// Replace Tokens for sending mail to tech
        /// </summary>
        /// <param name="_text"></param>
        /// <param name="_dateTime"></param>
        /// <param name="_date"></param>
        /// <param name="_startTime"></param>
        /// <param name="_endTime"></param>
        /// <param name="_jobID"></param>
        /// <param name="_jobTitle"></param>
        /// <param name="_price"></param>
        /// <param name="_description"></param>
        /// <param name="_techFirstName"></param>
        /// <param name="_techlLastName"></param>
        /// <param name="_techFullName"></param>
        /// <param name="_techCity"></param>
        /// <param name="_jobCity"></param>
        /// <param name="_jobState"></param>
        /// <param name="_techState"></param>
        /// <param name="_jobAddress"></param>
        /// <param name="_acceptLink"></param>
        /// <param name="_techRate"></param>
        /// <param name="_techPassword"></param>
        /// <param name="_techUserName"></param>
        /// <param name="_techEmailAddress"></param>
        /// <param name="_techID"></param>
        /// <param name="_declineLink"></param>
        /// <returns></returns>
        public static string MailMergeBytext(this string _text, string _dateTime = "", string _date = "", string _startTime = "", string _endTime = "", string _jobID = "", string _jobTitle = "", string _price = "", string _description = "", string _techFirstName = "", string _techlLastName = "", string _techFullName = "", string _techCity = "", string _jobCity = "", string _jobState = "", string _techState = "", string _jobAddress = "", string _acceptLink = "", string _techRate = "", string _techPassword = "", string _techUserName = "", string _techEmailAddress = "", string _techID = "", string _declineLink = "")
        {
            _text = _text.Replace("{{DeclineLink}}", _declineLink).Replace("{{AcceptLink}}", _acceptLink).Replace("{{JobAddress}}", _jobAddress).Replace("{{TechState}}", _techState).Replace("{{jobState}}", _jobState).Replace("{{jobCity}}", _jobCity).Replace("{{TechCity}}", _techCity).Replace("{{TechfullName}}", _techFullName).Replace("{{TechllastName}}", _techlLastName).Replace("{{TechfirstName}}", _techFirstName).Replace("{{Description}}", _description).Replace("{{Price}}", _price).Replace("{{techrate}}", _techRate).Replace("{{techPW}}", _techPassword).Replace("{{techID}}", _techID).Replace("{{techuser}}", _techUserName).Replace("{{techemail}}", _techEmailAddress).Replace("{{JobTitle}}", _jobTitle).Replace("{{JobId}}", _jobID).Replace("{{date}}", _date).Replace("{{StartTime}}", _startTime).Replace("{{EndTime}}", _endTime).Replace("{{datetime}}", _dateTime);
            return _text;
        }
        /// <summary>
        /// Validate Google Captcha
        /// </summary>
        /// <param name="EncodedResponse"></param>
        /// <returns></returns>
        public static string Validate(string EncodedResponse)
        {
            var client = new System.Net.WebClient();
            string PrivateKey = "6LfFi7sUAAAAAL3IgLuZRVjKdskSdd_PJk5Dk4_W";
            var GoogleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", PrivateKey, EncodedResponse));
            var captchaResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ReCaptchaClass>(GoogleReply);
            return captchaResponse.Success.ToLower();
        }
        /// <summary>
        /// Convert string into integer array
        /// </summary>
        /// <param name="_input"></param>
        /// <param name="_separator"></param>
        /// <returns></returns>
        public static int[] StringToArray(this string _input, string _separator)
        {
            string[] stringList = _input.Split(_separator.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            int[] list = new int[stringList.Length];
            for (int i = 0; i < stringList.Length; i++)
            {
                list[i] = Convert.ToInt32(stringList[i]);
            }
            return list;
        }
        /// <summary>
        /// Convert List to a comma seperated string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lst"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static string ToDelimitedString<T>(this IEnumerable<T> _list, string _separator = ", ")
        {
            return _list.ToDelimitedString(p => p, _separator);
        }
        public static string ToDelimitedString<S, T>(this IEnumerable<S> _list, Func<S, T> _selector, string _separator = ", ")
        {
            return string.Join(_separator, _list.Select(_selector));
        }
    }
    public class ReCaptchaClass
    {
        [JsonProperty("success")]
        public string Success
        {
            get { return m_Success; }
            set { m_Success = value; }
        }
        private string m_Success;
        [JsonProperty("error-codes")]
        public List<string> ErrorCodes
        {
            get { return m_ErrorCodes; }
            set { m_ErrorCodes = value; }
        }
        private List<string> m_ErrorCodes;
    }
}
