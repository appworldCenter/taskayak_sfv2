﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{
    public class PermissionServices
    {

        public List<PermissionRoleCountModal> GetPermissionRoleByCount(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            PermissionRoleModal _mdl = new PermissionRoleModal();
            List<PermissionRoleCountModal> _model = new List<PermissionRoleCountModal>();
            try
            {
                SortedList _list = new SortedList();
                _list.Add("@workspaceid", workspaceid);
                var dt = sqlHelper.fillDataTable("GetPermissionRoleByCount", "", _list);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PermissionRoleCountModal _item = new PermissionRoleCountModal();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.y = Convert.ToInt32(dt.Rows[i]["count"].ToString());
                    _item.color = dt.Rows[i]["color"].ToString();
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return _model;
        }


        public PermissionRoleModal GetAllActivePermissionRoles(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            PermissionRoleModal _mdl = new PermissionRoleModal();
            List<PermissionRoleItems> _model = new List<PermissionRoleItems>();
            SortedList _srt = new SortedList();
            _srt.Add("@workspaceid", workspaceid);
            try
            {
                var dt = sqlHelper.fillDataTable("GetAllActivePermissionRoles", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PermissionRoleItems _item = new PermissionRoleItems();
                    _item.Name = dt.Rows[i]["name"].ToString();
                    _item.PermissionRoleId = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._PermissionRoles = _model;
            return _mdl;
        }
        public PermissionRoleModal GetAllActivePermissionRole(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            PermissionRoleModal _mdl = new PermissionRoleModal();
            List<PermissionRoleItems> _model = new List<PermissionRoleItems>();
            SortedList _srt = new SortedList();
            _srt.Add("@workspcaeid", workspaceid);
            try
            {
                var dt = sqlHelper.fillDataTable("GetAllActivePermissionRole", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PermissionRoleItems _item = new PermissionRoleItems();
                    _item.Name = dt.Rows[i]["name"].ToString();
                    _item.PermissionRoleId = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.StatusId = Convert.ToInt32(dt.Rows[i]["statusid"].ToString());
                    _item.Status = dt.Rows[i]["status"].ToString();
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._PermissionRoles = _model;
            return _mdl;
        }
        public Dictionary<int, string> get_all_active_permissions()
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> _mdl = new Dictionary<int, string>();
            // List<RoleItems> _model = new List<RoleItems>()
            string name = "";
            int id = 0;
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_permissions_getall", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // RoleItems _item = new RoleItems();
                    name = dt.Rows[i]["name"].ToString();
                    id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _mdl.Add(id, name);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //   _mdl._roles = _model;
            return _mdl;
        }
        public List<PermissionsItem> GetAllActivePermissionsWithType()
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<PermissionsItem> _mdl = new List<PermissionsItem>();
            try
            {
                var dt = sqlHelper.fillDataTable("GetAllActivePermissionsWithType", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PermissionsItem _itm = new PermissionsItem();              // RoleItems _item = new RoleItems();
                    _itm.name = dt.Rows[i]["name"].ToString();
                    _itm.type = dt.Rows[i]["type"].ToString();
                    _itm.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _itm.Subtype = dt.Rows[i]["Subtype"].ToString();
                    _mdl.Add(_itm);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //   _mdl._roles = _model;
            return _mdl;
        }
        public string AddPermissionRole(PermissionRoleModal model, int userid, int workspaceid)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@PermissionRoleName", model.PermissionRole_name);
                _srt.Add("@statusid", model.statusid);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("AddPermissionRole", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        //AddSuperAdminRole
        public void add_newSuperAdminwith_role(int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@roleid", roleid);
                sqlHelper.executeNonQuery("AddSuperAdminRole", "", _srt);
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

        }
        public void add_newAdminwith_role(int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@roleid", roleid);
                sqlHelper.executeNonQuery("AddAdminRole", "", _srt);
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

        }
        public void add_newProjectmanagerwith_role(int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@roleid", roleid);
                sqlHelper.executeNonQuery("AddProjectManagerRole", "", _srt);
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

        }


        public void add_newDispatchwith_role(int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@roleid", roleid);
                sqlHelper.executeNonQuery("AdddispatchRole", "", _srt);
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

        }

        public void add_newTechwith_role(int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@roleid", roleid);
                sqlHelper.executeNonQuery("AddtechRole", "", _srt);
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

        }

        public int add_new_role(string role_name, int workspaceid)
        {
            int roleid = 0;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@rolename", role_name);
                _srt.Add("@workspaceid", workspaceid);
                roleid = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_role_addnew_v2", "", _srt).ToString());
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return roleid;
        }

        public string UpdatePermissionRole(PermissionRoleModal model, int userid)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@PermissionRoleName", model.PermissionRole_name);
                _srt.Add("@statusid", model.statusid);
                _srt.Add("@PermissionRoleId", model.PermissionRole_id);
                message = sqlHelper.executeNonQueryWMessage("UpdatePermissionRole", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }


        public string DeletePermissionsRole(int PermissionRoleId)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@PermissionRoleId", PermissionRoleId);
                message = sqlHelper.executeNonQueryWMessage("DeletePermissionsRole", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string GetUserType(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            string _model = "m";
            SortedList _srt = new SortedList();
            _srt.Add("@id", id);
            Crypto _crypt = new Crypto();
            try
            {
                var dt = sqlHelper.fillDataTable("GetMembertype", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    _model = dt.Rows[0]["type"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _model;

        }

        public List<int> GetAllActivePermissionsByPermissionRoleId(int PermissionRoleId, ref string name)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<int> _mdl = new List<int>();
            SortedList _srt = new SortedList();
            int id = 0;
            try
            {
                _srt.Add("@PermissionRoleId", PermissionRoleId);
                var ds = sqlHelper.fillDataSet("GetAllActivePermissionsByPermissionRoleId", "", _srt);
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    id = Convert.ToInt32(dt.Rows[i]["PermissionId"].ToString());
                    _mdl.Add(id);
                }
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    name = dt1.Rows[i]["PermissionRoleName"].ToString();
                }
                dt.Dispose();
                dt1.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //   _mdl._roles = _model;
            return _mdl;
        }

        public List<int> get_all_active_permissions_bytypeid(int typeid, int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<int> _mdl = new List<int>();
            SortedList _srt = new SortedList();
            int id = 0;
            try
            {
                _srt.Add("@typeid", typeid);
                _srt.Add("@roleid", roleid);
                var dt = sqlHelper.fillDataTable("tbl_permissions_get_byroleid_typeid", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    id = Convert.ToInt32(dt.Rows[i]["PermissionId"].ToString());
                    _mdl.Add(id);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //   _mdl._roles = _model;
            return _mdl;
        }

        public bool get_all_active_permissions_bypermissionId(int pid, int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            bool _mdl = false;
            SortedList _srt = new SortedList();
            try
            {
                _srt.Add("@pid", pid);
                _srt.Add("@roleid", roleid);
                _mdl = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_notification_tbl_permissions_get_byroleid_typeid", "", _srt).ToString()) > 0 ? true : false;

                //  dt.Dispose();
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //   _mdl._roles = _model;
            return _mdl;
        }
        public string UpdatePermissionRule(int[] permissionids, int RuleId)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@permissionids", string.Join<int>(",", permissionids));
                _srt.Add("@RuleId", RuleId);
                message = sqlHelper.executeNonQueryWMessage("UpdatePermissionRule", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string DeletePermissionRule(int RuleId)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@RuleId", RuleId);
                message = sqlHelper.executeNonQueryWMessage("DeletePermissionRule", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

    }
}
