﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{
    public class ReminderServices
    {

        public void AddReminderForSend(int _reminderId, bool _needConfirmation, string _body, String _type, DateTime _sentDate, int _userId, int _jobId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@reminderId", _reminderId);
                sortedLists.Add("@needConfirmation", _needConfirmation);
                sortedLists.Add("@body", _body);
                sortedLists.Add("@type", _type);
                sortedLists.Add("@sentDate", _sentDate);
                sortedLists.Add("@userId", _userId);
                sortedLists.Add("@jobId", _jobId);
                sqlHelper.executeNonQuery("AddReminderForSend", "", sortedLists);
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
        }
    }
}
