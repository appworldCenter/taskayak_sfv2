﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{
    public class accountservices
    {
        public int validateLogin(LoginModel _login, ref string message, ref int roleid, ref bool isresetneeded, ref bool _status, ref bool accepttermsrqud, ref int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto _crypt = new Crypto();
            isresetneeded = false;
            _status = true;
            int id = 0;
            roleid = 0;
            int statusid = 0;
            message = "Error : Password not match";
            try
            {
                string passowrd = "";
                sortedLists.Add("@userName", _login.UserName);
                sortedLists.Add("@workspacename", _login.Workspace);
                var dt = sqlHelper.fillDataTable("tbl_member_getlogin_v2", "", sortedLists);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        passowrd = _crypt.DecryptStringAES(dt.Rows[i]["Password"].ToString());
                        if (passowrd == _login.Password)
                        {
                            id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                            statusid = Convert.ToInt32(dt.Rows[i]["member_status_id"].ToString());
                            roleid = Convert.ToInt32(dt.Rows[i]["roleid"].ToString());
                            isresetneeded = Convert.ToBoolean(dt.Rows[i]["resetneeded"].ToString());
                            _status = statusid == 1007 ? Convert.ToBoolean(dt.Rows[i]["isprofilecompleted"].ToString()) : true;
                            accepttermsrqud = Convert.ToBoolean(dt.Rows[i]["accepttermsrqud"].ToString());
                            workspaceid = Convert.ToInt32(dt.Rows[i]["workspaceid"].ToString());
                            break;
                        }
                    }
                }
                else
                {
                    message = "Error : Password not match";
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error :" + exception.Message;
                id = 0;
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return id;
        }


        public string UpdateBilling(billingModal _model, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string _res = "Account updated successfully";
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);

                sortedLists.Add("@Address", _model.Address);
                sortedLists.Add("@Email", _model.Email);

                sortedLists.Add("@Primarycontact", _model.Primarycontact);
                sortedLists.Add("@Name", _model.Name);
                sortedLists.Add("@Telephone", _model.Telephone);
                _res = sqlHelper.executeNonQueryWMessage("update_Billing", "", sortedLists).ToString();
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return _res;
        }

        public string Updatepacjkage(int workspaceid, int smsCount, int emailcount, string subscriptionId, string CustomerId, string PlanName)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string _res = "Account updated successfully";
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                sortedLists.Add("@Count", smsCount);
                sortedLists.Add("@CountType", emailcount);
                sortedLists.Add("@subscriptionId", subscriptionId);
                sortedLists.Add("@CustomerId", CustomerId);
                sortedLists.Add("@PlanName", PlanName);
                _res = sqlHelper.executeNonQueryWMessage("addnewsubscription", "", sortedLists).ToString();
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return _res;
        }

        public string UpdateBillingwithupgrade(string accounttype, int workspaceid, int email, int sms, string customerid, string subscriptionid, int planid, int userid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string _res = "Account updated successfully";
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                sortedLists.Add("@accounttype", accounttype);
                sortedLists.Add("@emailcount", email);
                sortedLists.Add("@smscount", sms);
                sortedLists.Add("@customerid", customerid);
                sortedLists.Add("@subscriptionid", subscriptionid);
                sortedLists.Add("@planid", planid);
                sortedLists.Add("@userid", userid);

                _res = sqlHelper.executeNonQueryWMessage("update_Billing_upgradeaccount", "", sortedLists).ToString();
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return _res;
        }

        public string deactivateaccount(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string _res = "Account updated successfully";
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                _res = sqlHelper.executeNonQueryWMessage("Deactivateaccount", "", sortedLists).ToString();
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return _res;
        }

        public string getaccounttype(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string _res = "paid";
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                _res = sqlHelper.executeNonQueryWMessage("getaccounttype", "", sortedLists).ToString();
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return _res;
        }

        public billingModal getBilling(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            billingModal _model = new billingModal();
            List<subscriptions> pcks = new List<subscriptions>();
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                var ds = sqlHelper.fillDataSet("GetBilling", "", sortedLists);
                var dt = ds.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _model.AccountNumber = dt.Rows[i]["AccountNumber"].ToString();
                    _model.AccountType = dt.Rows[i]["ServiceName"].ToString();
                    _model.Address = dt.Rows[i]["Address"].ToString();
                    _model.Email = dt.Rows[i]["Email"].ToString();
                    _model.EmailCount = Convert.ToInt32(dt.Rows[i]["EmailCount"].ToString());
                    _model.smsCount = Convert.ToInt32(dt.Rows[i]["SmsCount"].ToString());
                    _model.Primarycontact = dt.Rows[i]["Primarycontact"].ToString();
                    _model.Name = dt.Rows[i]["Name"].ToString();
                    _model.Telephone = dt.Rows[i]["phonenumber"].ToString();
                }
                var dt1 = ds.Tables[1];
                dt.Dispose();
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    subscriptions _p = new subscriptions();
                    _p.smsCount = Convert.ToInt32(dt1.Rows[i]["Count"].ToString());
                    _p.emailcount = Convert.ToInt32(dt1.Rows[i]["CountType"].ToString());
                    _p.subscriptionId = dt1.Rows[i]["subscriptionId"].ToString();
                    _p.Planname = dt1.Rows[i]["PlanName"].ToString();
                    pcks.Add(_p);
                }
                dt1.Dispose();
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _model.packages = pcks;
            return _model;
        }
        public bool validate_email(string email, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            bool isexist = false;
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                sortedLists.Add("@email", email);
                var dt = sqlHelper.fillDataTable("tbl_member_validate_email", "", sortedLists);
                if (dt.Rows.Count > 0)
                {
                    isexist = true;
                }
                dt.Dispose();
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return isexist;
        }

        public bool validate_memberid(string memberid, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            bool isexist = false;
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                sortedLists.Add("@memberid", memberid);
                var dt = sqlHelper.fillDataTable("tbl_member_validate_memberid", "", sortedLists);
                if (dt.Rows.Count > 0)
                {
                    isexist = true;
                }
                dt.Dispose();
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return isexist;
        }

        public bool validate_username(string username, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            bool isexist = false;
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                sortedLists.Add("@username", username);
                var dt = sqlHelper.fillDataTable("tbl_member_validate_username", "", sortedLists);
                if (dt.Rows.Count > 0)
                {
                    isexist = true;
                }
                dt.Dispose();
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return isexist;
        }


        public string reset_password(string username, string tempasswors, ref string name, ref string email)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string isexist = "";
            name = "";
            email = "";
            try
            {
                sortedLists.Add("@username", username);
                sortedLists.Add("@temppassword", tempasswors);
                var dt = sqlHelper.fillDataTable("tbl_member_reset_password", "", sortedLists);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    isexist = dt.Rows[i]["message"].ToString();
                    email = dt.Rows[i]["EMail"].ToString();
                    name = dt.Rows[i]["name"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return isexist;
        }

        public string update_password(int memberid, string password)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string isexist = "";
            try
            {
                sortedLists.Add("@memberid", memberid);
                sortedLists.Add("@password", password);
                isexist = sqlHelper.executeNonQueryWMessage("tbl_member_update_password", "", sortedLists).ToString();

            }
            catch (Exception ex)
            {
                isexist = "Error:" + ex.Message;
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return isexist;
        }

        public string validatepassword(string c_password, string n_password, int memberid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto _crypt = new Crypto();
            string message = "Error : Current password is not match";
            try
            {
                string passowrd = "";
                sortedLists.Add("@id", memberid);
                var dt = sqlHelper.fillDataTable("tbl_member_get_passordbyId", "", sortedLists);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        passowrd = _crypt.DecryptStringAES(dt.Rows[i]["Password"].ToString());
                        if (passowrd == c_password)
                        {
                            n_password = _crypt.EncryptStringAES(n_password);
                            sortedLists.Add("@password", n_password);
                            sqlHelper.executeNonQuery("tbl_member_update_passordbyId", "", sortedLists);
                            message = "Password Updated Successfully";
                        }
                    }
                }
                else
                {
                    message = "Error : Current password is not match";
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error :" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return message;
        }

    }
}
