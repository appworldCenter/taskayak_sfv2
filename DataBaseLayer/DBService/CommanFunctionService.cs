﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{/// <summary>
 /// Default service
 /// </summary>
    public class defaultservices
    {
        public Dictionary<int, string> GetAllActiveState()
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("tbl_state_getallactivestate_without_select2", "", null);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }


        public int savegetCityId(int stateid = 0, string city = "")
        {
            SqlHelper sqlHelper = new SqlHelper();
            int cityId = 0;
            SortedList sortedLists = new SortedList();
            sortedLists.Add("@city", city);
            sortedLists.Add("@stateid", stateid);
            try
            {
                if (string.IsNullOrEmpty(city))
                {
                    cityId = 0;
                }
                else
                {
                    cityId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("GetcityIdbyname", "", sortedLists).ToString());

                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return cityId;
        }

        public int CreateCity(ref int stateId, string state = "", string city = "")
        {
            SqlHelper sqlHelper = new SqlHelper();
            int cityId = 0;
            stateId = GetStateId(state);
            SortedList sortedLists = new SortedList();
            sortedLists.Add("@city", city);
            sortedLists.Add("@stateid", stateId);
            try
            {
                if (string.IsNullOrEmpty(city))
                {
                    cityId = 0;
                }
                else
                {
                    cityId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("GetcityIdbyname", "", sortedLists).ToString());

                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return cityId;
        }

        public int GetStateId(string state)
        {
            SqlHelper sqlHelper = new SqlHelper();
            int stateId = 0;
            SortedList sortedLists = new SortedList();
            sortedLists.Add("@state", state);
            try
            {
                stateId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("GetstateIdbyname", "", sortedLists).ToString());
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return stateId;
        }

        public int GetmemberId(string name)
        {
            SqlHelper sqlHelper = new SqlHelper();
            int stateId = 0;
            SortedList sortedLists = new SortedList();
            sortedLists.Add("@name", name);
            try
            {
                stateId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("GetmemberIdbyname", "", sortedLists).ToString());
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return stateId;
        }

        public int GetrateId(string rate)
        {
            SqlHelper sqlHelper = new SqlHelper();
            int stateId = 0;
            SortedList sortedLists = new SortedList();

            sortedLists.Add("@rate", rate);
            try
            {
                stateId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("GetrateIdbyname", "", sortedLists).ToString());
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return stateId;
        }

        public Dictionary<int, string> get_all_skills(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_skill_getallactiveskill_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }
        public Dictionary<int, string> get_all_active_member(string type = "M", bool issubcontractor = false, int userid = 0, int Workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@type", type); sortedLists.Add("@Workspaceid", Workspaceid);
                sortedLists.Add("@issubcontractor", issubcontractor);
                sortedLists.Add("@userid", userid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallactivemember_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString().ToTitleCase());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_active_member_byid(int id, string type, int Worspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@Id", id);
                sortedLists.Add("@type", type);
                sortedLists.Add("@Workspaceid", Worspaceid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallactivemember_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_active_manager_byid(int id, string type, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@Id", id);
                sortedLists.Add("@workspaceid", workspaceid);
                sortedLists.Add("@type", type);
                DataTable dataTable = sqlHelper.fillDataTable("Getmanagerbyuser", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["userId"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_active_member_bycityid(int id, int Workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@Id", id);
                sortedLists.Add("@Workspaceid", Workspaceid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallactivemember_bycityid", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString().ToTitleCase());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_active_memberid(int cid = 0, int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            sortedLists.Add("@id", cid);
            sortedLists.Add("@workspaceid", workspaceid);
            try
            {

                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallmemberid", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["member_id"].ToString().ToTitleCase());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_membertype_projectmanager(int Worspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            sortedLists.Add("@Worspaceid", Worspaceid);
            try
            {

                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_Getprojectmanager", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["name"].ToString().ToTitleCase());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }


        public Dictionary<int, string> get_all_active_memberbycompanyid(int id, int Worspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                sortedLists.Add("@Workspaceid", Worspaceid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getmemberBycompanyId", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["member_id"].ToString().ToTitleCase());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }
        public Dictionary<int, string> get_all_active_project(int id, int Worspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                sortedLists.Add("@Workspaceid", Worspaceid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_project_getmemberBycompanyId", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["ProjectId"].ToString()), dataTable.Rows[i]["ProjectName"].ToString().ToTitleCase());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }
        public Dictionary<int, string> get_all_active_member_bystateid(int id, int Workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@Id", id);
                sortedLists.Add("@Workspaceid", Workspaceid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallactivemember_bystateid", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString().ToTitleCase());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }
        public Dictionary<int, string> get_all_city_by_stateid(int stateid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", stateid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_state_getallactivecity_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_job(int Worspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            sortedLists.Add("@Worspaceid", Worspaceid);
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("tbl_job_activejobs_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_assigned_jobs(int id, bool forpay, int Worspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                sortedLists.Add("@forpay", forpay);
                sortedLists.Add("@Worspaceid", Worspaceid);

                DataTable dataTable = sqlHelper.fillDataTable("tbl_job_activejobs_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }
        public Dictionary<int, string> get_all_client(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_clients_getallactiveclient_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_manager_byid(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_projectmanager_getallactiveprojectmanager_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_manager_byuserid(int id, int Worspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                sortedLists.Add("@Worspaceid", Worspaceid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_projectmanager_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }



        public Dictionary<int, string> get_all_template(ref int templateid, int Worspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@Workspaceid", Worspaceid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_template_getalltemplateBySelect", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                    if (Convert.ToBoolean(dataTable.Rows[i]["isdefault"].ToString()) == true)
                    {
                        templateid = Convert.ToInt32(dataTable.Rows[i]["id"].ToString());
                    }
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }


        public Dictionary<int, string> get_all_job_OffersByStateId(int stateId, int Worspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", stateId);
                sortedLists.Add("@Workspaceid", Worspaceid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_job_offerGetByState", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());

                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }
        //
        //

        public Dictionary<int, string> get_all_offer_unassign(int stateid = 0, string city = "", string jobid = "", int clientid = 0, int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                city = city == "null" ? "" : city;
                if (!string.IsNullOrEmpty(city) && city != "0")
                {
                    sortedLists.Add("@cityid", city);
                }
                if (stateid != 0)
                {
                    sortedLists.Add("@stateid", stateid);
                }
                if (clientid != 0)
                {
                    sortedLists.Add("@clientid", clientid);
                }
                if (!string.IsNullOrEmpty(jobid) && jobid != "0")
                {
                    sortedLists.Add("@offerid", jobid);
                }
                DataTable dataTable = sqlHelper.fillDataTable("tbl_job_offerGetByfilter", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());

                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_job_unassign(int stateid = 0, string city = "", string jobid = "", int clientid = 0, int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                city = city == "null" ? "" : city;
                if (!string.IsNullOrEmpty(city) && city != "0")
                {
                    sortedLists.Add("@id", city);
                }
                if (stateid != 0)
                {
                    sortedLists.Add("@stateid", stateid);
                }
                if (clientid != 0)
                {
                    sortedLists.Add("@clientid", clientid);
                }
                if (!string.IsNullOrEmpty(jobid))
                {
                    sortedLists.Add("@jobid", jobid);
                }
                DataTable dataTable = sqlHelper.fillDataTable("tbl_job_Getunassign", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());

                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_job_OffersBycity(string city, int Worspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                if (!string.IsNullOrEmpty(city))
                {
                    sortedLists.Add("@id", city);
                }
                sortedLists.Add("@Workspaceid", Worspaceid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_job_offerGetBycity", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());

                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }


    }
}
