﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
 
namespace DataBaseLayer.DBService
{
    public class partnerService
    {
        public void addclientsignupnotification(int newclientid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _list = new SortedList();
            _list.Add("@newclientid", newclientid);
            try
            {
                sqlHelper.executeNonQuery("AddclientsignupNotification", "", _list);
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // return memberid;
        }
        public string get_rate(int memberId)
        {
            string message = "$1";
            SqlHelper sqlHelper = new SqlHelper();

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberId);
                message = sqlHelper.executeNonQueryWMessage("tbl_clients_getrate", "", _srt).ToString().Currency();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string add_new_partner(partnermodal_item partner, int userid, int workspaceid)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@partnername", partner.name);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@primarycontact", partner.primarycontactname);
                _srt.Add("@rate", string.IsNullOrEmpty(partner.rate) ? "$0" : partner.rate);
                _srt.Add("@Address", partner.MapValidateAddress);
                //  _srt.Add("@Cityid", partner.city_id);
                //  _srt.Add("@Stateid", partner.state_id);
                _srt.Add("@suite", partner.suite);
                // _srt.Add("@Pincode", partner.zipcode);
                _srt.Add("@Email", partner.email);
                _srt.Add("@Phone", partner.phone);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("add_new_partner", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }



        public string update_partners(partnermodal_item partner)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@partnername", partner.name);
                _srt.Add("@primary", partner.primarycontactname);
                _srt.Add("@rate", string.IsNullOrEmpty(partner.rate) ? "$0" : partner.rate);
                _srt.Add("@Address", partner.MapValidateAddress);
                //_srt.Add("@Cityid", partner.city_id);
                //_srt.Add("@Stateid", partner.state_id);
                _srt.Add("@suite", partner.suite);
                // _srt.Add("@Pincode", partner.zipcode);
                _srt.Add("@Email", partner.email);
                _srt.Add("@Phone", partner.phone);
                _srt.Add("@partnerid", partner.partnerid);
                message = sqlHelper.executeNonQueryWMessage("update_partners", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public clientmodal get_all_active_clients()
        {
            SqlHelper sqlHelper = new SqlHelper();
            clientmodal _mdl = new clientmodal();
            List<clientmodal_item> _model = new List<clientmodal_item>();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_client_getallclients", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    clientmodal_item _item = new clientmodal_item();
                    _item.name = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.clientid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl.clients = _model;
            return _mdl;
        }

        public List<partnermodal_item> get_all_active_partners_bypaging(ref int total, int startindex = 0, int endindex = 25, string search = "", int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            partnermodal _mdl = new partnermodal();
            List<partnermodal_item> _model = new List<partnermodal_item>();
            Crypto _crypt = new Crypto();
            SortedList _srtlist = new SortedList();
            _srtlist.Add("@startindex", startindex);
            _srtlist.Add("@endIndex", endindex);
            _srtlist.Add("@workspaceid", workspaceid);
            if (!string.IsNullOrEmpty(search))
            {
                _srtlist.Add("@search", search);
            }
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_partners_getallpartnerss_by_paging", "", _srtlist);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    partnermodal_item _item = new partnermodal_item();
                    _item.name = dt.Rows[i]["Partnername"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.partnerid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl.clients = _model;
            return _model;
        }

        public partnermodal_item get_all_active_partners_byid(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            partnermodal_item _item = new partnermodal_item();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@partnerid", id);
                var dt = sqlHelper.fillDataTable("get_all_active_partners_byid", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // clientmodal_item _item = new clientmodal_item();
                    _item.name = dt.Rows[i]["Partnername"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    //_item.city = dt.Rows[i]["city"].ToString();
                    // _item.state = dt.Rows[i]["state"].ToString();
                    _item.suite = dt.Rows[i]["SuiteNumber"].ToString();
                    _item.MapValidateAddress = dt.Rows[i]["Address"].ToString();
                    // _item.city_id = Convert.ToInt32(dt.Rows[i]["Cityid"].ToString());
                    // _item.state_id = Convert.ToInt32(dt.Rows[i]["Stateid"].ToString());
                    //  _item.zipcode = dt.Rows[i]["Pincode"].ToString();
                    _item.partnerid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    // _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //_mdl.clients = _model;
            return _item;
        }

        public string delete_partner(int partnerid)
        {
            string message = "Partner Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@partnerid", partnerid);
                sqlHelper.executeNonQuery("tbl_partners_delete_partners", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }
    }
}
