﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{
    public class notificationServices
    {
        public int GetNotificationCountByType(int MainId, NotificationType type, int userId)
        {
            int count = 0;
            int _typeId = 1;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                switch (type)
                {
                    case NotificationType.Offer:
                        _typeId = 2;
                        break;
                    case NotificationType.Pay:
                        _typeId = 3;
                        break;
                }
                _srt.Add("@id", MainId);
                _srt.Add("@userId", userId);
                _srt.Add("@type", _typeId);
                count = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_notification_get_count", "", _srt).ToString());
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return count;
        }
        public bool CheckreadStatusById(int messageId)
        {
            bool isread = false;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", messageId);
                isread = Convert.ToBoolean(sqlHelper.executeNonQueryWMessage("tbl_notification_get_count", "", _srt).ToString());
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return isread;
        }

        public void update_jobreadofferstatus(int id, int userid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                _srt.Add("@offerid", userid);
                sqlHelper.executeNonQuery("tbl_notification_updateJobNotification2", "", _srt);
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            // return message;
        }
        public void update_jobreadstatus(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                sqlHelper.executeNonQuery("tbl_notification_updateJobNotification", "", _srt);
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            // return message;
        }
        public void update_jobreadstatus2(int id, int receiver, string Proc = "tbl_notification_updateJobNotification2")
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                _srt.Add("@receiver", receiver);
                //
                sqlHelper.executeNonQuery(Proc, "", _srt);
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            // return message;
        }
        public List<messageModal> get_all_active_notificationsbyjovbid(int id, int userId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<messageModal> _model = new List<messageModal>();
            List<int> _MessageIds = new List<int>();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                _srt.Add("@userId", userId);
                var dt = sqlHelper.fillDataTable("tbl_notification_get_notifications_jobs", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    messageModal _mdl = new messageModal();
                    _mdl.mainMessageid = Convert.ToInt32(dt.Rows[i]["mesgid"].ToString());
                    if (!_MessageIds.Contains(_mdl.mainMessageid))
                    {
                        _MessageIds.Add(_mdl.mainMessageid);
                        _mdl.body = dt.Rows[i]["body"].ToString();
                        _mdl.createdby = dt.Rows[i]["sender"].ToString();
                        _mdl.createdDate = dt.Rows[i]["Createddate"].ToString();
                        _mdl.isread = Convert.ToBoolean(dt.Rows[i]["isread"].ToString());
                        _mdl._createdby = Convert.ToInt32(dt.Rows[i]["createdby"].ToString());
                        _mdl.msgitem = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                        if (_mdl._createdby == userId)
                        {
                            _mdl.isread = true;
                        }
                        _model.Add(_mdl);
                    }

                }
                dt.Dispose();
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _model;
        }

        public List<messageModal> get_all_active_notificationsbyofferid(int id, int userId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<messageModal> _model = new List<messageModal>(); List<int> _MessageIds = new List<int>();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                _srt.Add("@userId", userId);
                var dt = sqlHelper.fillDataTable("tbl_notification_get_notifications_offer", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    messageModal _mdl = new messageModal(); _mdl.mainMessageid = Convert.ToInt32(dt.Rows[i]["mesgid"].ToString());
                    if (!_MessageIds.Contains(_mdl.mainMessageid))
                    {
                        _mdl.body = dt.Rows[i]["body"].ToString();
                        _mdl.createdby = dt.Rows[i]["sender"].ToString();
                        _mdl.createdDate = dt.Rows[i]["Createddate"].ToString();
                        _mdl.isread = Convert.ToBoolean(dt.Rows[i]["isread"].ToString());
                        _mdl.msgitem = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                        if (_mdl._createdby == userId)
                        {
                            _mdl.isread = true;
                        }
                        _model.Add(_mdl);
                    }
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _model;
        }
    }
}
