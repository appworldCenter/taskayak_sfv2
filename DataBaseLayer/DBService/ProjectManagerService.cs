﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{
    public class prjmanagerservices
    {
        public List<prjmngr_item> get_all_active_prj_byclient(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            prjmngrmodal _mdl = new prjmngrmodal();
            List<prjmngr_item> _model = new List<prjmngr_item>();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                var dt = sqlHelper.fillDataTable("tbl_projectmanager_getallprjs", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    prjmngr_item _item = new prjmngr_item();
                    _item.name = dt.Rows[i]["name"].ToString().ToTitleCase();
                    _item.phone = dt.Rows[i]["Phonenumber"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();

                    _item.prjmanagerid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl.clients = _model;
            return _model;
        }

        public List<prjmngr_item> GetAllActiveManagerByClientIdWithPaging(ref int total, int id, int startIndex, int endIndex, string search = "")
        {
            SqlHelper sqlHelper = new SqlHelper();
            prjmngrmodal _mdl = new prjmngrmodal();
            List<prjmngr_item> _model = new List<prjmngr_item>();
            Crypto _crypt = new Crypto();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                _srt.Add("@startIndex", startIndex);
                _srt.Add("@endIndex", endIndex);
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                var dt = sqlHelper.fillDataTable("tbl_projectmanager_getallprjs_by_index", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    prjmngr_item _item = new prjmngr_item();
                    _item.name = dt.Rows[i]["name"].ToString().ToTitleCase();
                    _item.phone = dt.Rows[i]["Phonenumber"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.prjmanagerid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.clientid = id;
                    _item.prj_token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _item.clt_token = _crypt.EncryptStringAES(id.ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl.clients = _model;
            return _model;
        }
        public List<prjmngr_item> get_all_active_prj_bypartner_byindex(ref int total, int id, int startIndex, int endIndex, string search = "", int Workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            prjmngrmodal _mdl = new prjmngrmodal();
            List<prjmngr_item> _model = new List<prjmngr_item>();
            Crypto _crypt = new Crypto();
            try
            {
                SortedList _srt = new SortedList();
                // _srt.Add("@Workspaceid", Workspaceid);
                _srt.Add("@id", id);
                _srt.Add("@startIndex", startIndex);
                _srt.Add("@endIndex", endIndex);
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                var dt = sqlHelper.fillDataTable("tbl_projectmanager_getall_partner_by_index", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    prjmngr_item _item = new prjmngr_item();
                    _item.name = dt.Rows[i]["name"].ToString().ToTitleCase();
                    _item.phone = dt.Rows[i]["Phonenumber"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.prjmanagerid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.clientid = id;
                    _item.prj_token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _item.clt_token = _crypt.EncryptStringAES(id.ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl.clients = _model;
            return _model;
        }

        public string GetPosition(string Type)
        {
            string Position = "Technician";
            switch (Type)
            {
                case "ST":
                    Position = "Technician";
                    break;
                case "LT":
                    Position = "Lead Technician";
                    break;
                case "S":
                    Position = "Subcontractor";
                    break;
            }
            return Position;
        }

        public List<ContractorProjectManager_Item> GetAllActiveProjectManagerByContractorByIndex(ref int total, int id, int startIndex, int endIndex, string search = "")
        {
            SqlHelper sqlHelper = new SqlHelper();
            prjmngrmodal _mdl = new prjmngrmodal();
            List<ContractorProjectManager_Item> _model = new List<ContractorProjectManager_Item>();
            Crypto _crypt = new Crypto();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                _srt.Add("@startIndex", startIndex);
                _srt.Add("@endIndex", endIndex);
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                var dt = sqlHelper.fillDataTable("GetAllActiveProjectManagerByContractorByIndex", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ContractorProjectManager_Item _item = new ContractorProjectManager_Item();
                    _item.name = dt.Rows[i]["name"].ToString().ToTitleCase();
                    _item.phone = dt.Rows[i]["Phonenumber"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.position = GetPosition(dt.Rows[i]["position"].ToString());
                    _item.manager = dt.Rows[i]["manager"].ToString();
                    _item.prjmanagerid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.clientid = id;
                    _item.prj_token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _item.clt_token = _crypt.EncryptStringAES(id.ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl.clients = _model;
            return _model;
        }

        public string CreateProjectManager(prjmngr_item _request, int userid, int workspaceid)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@name", _request.name);
                _srt.Add("@clientid", _request.clientid);
                _srt.Add("@Email", _request.email);
                _srt.Add("@Phone", _request.phone);
                _srt.Add("@createdby", userid);
                _srt.Add("@workspaceid", workspaceid);
                message = sqlHelper.executeNonQueryWMessage("CreateProjectManager", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string add_new_partner_manager(prjmngr_item prjectmgr, int userid)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@name", prjectmgr.name);
                _srt.Add("@PartnerId", prjectmgr.partnerid);
                _srt.Add("@Email", prjectmgr.email);
                _srt.Add("@Phone", prjectmgr.phone);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl__partner_manager_add", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string UpdateManagerProfile(prjmngr_item _request)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@name", _request.name);
                _srt.Add("@Email", _request.email);
                _srt.Add("@Phone", _request.phone);
                _srt.Add("@managerid", _request.prjmanagerid);
                message = sqlHelper.executeNonQueryWMessage("tbl_manager_update", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update__partner_manager(prjmngr_item prjectmgr)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@name", prjectmgr.name);
                _srt.Add("@Email", prjectmgr.email);
                _srt.Add("@Phone", prjectmgr.phone);
                _srt.Add("@managerid", prjectmgr.prjmanagerid);
                message = sqlHelper.executeNonQueryWMessage("tbl__partner_manager_update", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string DeleteManager(int managerid)
        {
            string message = string.Empty;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@managerid", managerid);
                message = sqlHelper.executeNonQueryWMessage("DeleteManager", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string delete_partner_manager(int managerid)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@managerid", managerid);
                message = sqlHelper.executeNonQueryWMessage("tbl_partner_manager_remove", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
    }
}
