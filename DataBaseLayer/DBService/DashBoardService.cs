﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
namespace DataBaseLayer.DBService
{
    public class dashboardservices
    {
        public dashboardmodal get_all_active_dashboard(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            dashboardmodal _mdl = new dashboardmodal();
            List<dashboardjobs> _js = new List<dashboardjobs>();
            List<dashboardoffer> _ofer = new List<dashboardoffer>();
            List<ratejobs> _pay = new List<ratejobs>();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@workspaceid", workspaceid);
                var ds = sqlHelper.fillDataSet("get_dashboard", "", _srtlist);
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];
                var dt3 = ds.Tables[3];
                var dt4 = ds.Tables[4];
                double monthpay = 0;
                double revenue = 0;
                double rate = 0;
                double hrsworked = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _mdl.member = Convert.ToInt32(dt.Rows[i]["m"].ToString());
                    _mdl.jobs = Convert.ToInt32(dt.Rows[i]["j"].ToString());
                    _mdl.leads = Convert.ToInt32(dt.Rows[i]["l"].ToString());
                }
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    rate = Convert.ToDouble(dt1.Rows[i]["rate"].ToString().Replace("$", ""));
                    hrsworked = Convert.ToDouble(dt1.Rows[i]["Hoursworked"].ToString());
                    monthpay = monthpay + (rate * hrsworked);
                }
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    dashboardjobs _itm = new dashboardjobs();
                    _itm.clientname = dt2.Rows[i]["Clientname"].ToString();
                    _itm.Technician = dt2.Rows[i]["Technician"].ToString().ToTitleCase();
                    _itm.startdate = Convert.ToDateTime(dt2.Rows[i]["startdate"].ToString()).ToString("dd-MM-yyyy");
                    _itm.rate = dt2.Rows[i]["Client_rate"].ToString();
                    _itm.Jobid = dt2.Rows[i]["Jobid"].ToString();
                    _js.Add(_itm);
                }
                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    dashboardoffer _itm = new dashboardoffer();
                    _itm.clientname = dt3.Rows[i]["Clientname"].ToString();
                    _itm.Technician = dt3.Rows[i]["dispatcher"].ToString().ToTitleCase();
                    _itm.startdate = Convert.ToDateTime(dt3.Rows[i]["startdate"].ToString()).ToString("dd-MM-yyyy");
                    _itm.rate = dt3.Rows[i]["Client_rate"].ToString();
                    _itm.Jobid = dt3.Rows[i]["Jobid"].ToString();
                    _ofer.Add(_itm);
                }
                for (int i = 0; i < dt4.Rows.Count; i++)
                {
                    rate = Convert.ToDouble(dt4.Rows[i]["rate"].ToString().Replace("$", ""));
                    hrsworked = Convert.ToDouble(dt4.Rows[i]["Hoursworked"].ToString());
                    revenue = revenue + (rate * hrsworked);
                }
                _mdl.monthuypay = monthpay.ToString().Currency();
                _mdl.monthreveneue = revenue.ToString().Currency();
                dt.Dispose();
                dt3.Dispose();
                dt2.Dispose();
                ds.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._jobs = _js;
            _mdl._offer = _ofer;
            return _mdl;
        }


        public dashboardmodaljson get_all_active_dashboard_json(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            dashboardmodaljson _mdl = new dashboardmodaljson();
            List<int> _js = new List<int>();
            List<int> _ofer = new List<int>();
            int count = 1;
            try
            {
                SortedList _list = new SortedList();
                _list.Add("@workspaceid", workspaceid);
                var ds = sqlHelper.fillDataSet("getjobofferdata", "", _list);
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                int startcoulm = 0;
                for (int i = 1; i < 13; i++)
                {
                    if (startcoulm <= dt.Rows.Count)
                    {
                        try
                        {
                            count = Convert.ToInt32(dt.Rows[startcoulm]["months"].ToString());
                            if (count == i)
                            {
                                _js.Add(Convert.ToInt32(dt.Rows[startcoulm]["total"].ToString()));
                                startcoulm = startcoulm + 1;
                            }
                            else
                            {
                                _js.Add(0);
                            }
                        }
                        catch
                        {
                            _js.Add(0);
                        }
                    }
                    else
                    {
                        _js.Add(0);

                    }
                }
                startcoulm = 0;
                for (int i = 1; i < 13; i++)
                {
                    if (startcoulm <= dt1.Rows.Count)
                    {
                        try
                        {
                            count = Convert.ToInt32(dt1.Rows[startcoulm]["months"].ToString());
                            if (count == i)
                            {
                                _ofer.Add(Convert.ToInt32(dt1.Rows[startcoulm]["total"].ToString()));
                                startcoulm = startcoulm + 1;
                            }
                            else
                            {
                                _ofer.Add(0);
                            }
                        }
                        catch { _ofer.Add(0); }
                    }
                    else
                    {
                        _ofer.Add(0);
                    }
                }
                dt.Dispose();
                ds.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._jobs = _js;
            _mdl._offer = _ofer;
            return _mdl;
        }
    }
}
