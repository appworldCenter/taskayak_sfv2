﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{
    public class domainservices
    {
        public int validate_domain(string name)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            int domainID = 0;
            try
            {
                sortedLists.Add("@name", name);
                domainID = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("getworkspaceidByname", "", sortedLists).ToString());
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return domainID;
        }



        public int createnewworkspace(string name)
        {
            SqlHelper sqlHelper = new SqlHelper();
            int _model = 0;
            SortedList _srt = new SortedList();
            _srt.Add("@name", name);
            Crypto _crypt = new Crypto();
            try
            {
                if (name.ToLower() == "portal" || name.ToLower() == "www" || name.ToLower() == "taskayak")
                {
                    _model = 0;
                }
                else
                {
                    _model = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("Addnewwebspace", "", _srt).ToString());

                }
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _model;

        }

    }
}
