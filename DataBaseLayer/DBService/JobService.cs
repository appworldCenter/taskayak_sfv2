﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{
    public class jobservices
    {
        public string AddNewJob(job _mdl, int userid, string tecrate, string clientrate, string MapValidateAddress, string zip, int workspaceid, int projectId)
        {
            string message = "Job Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string _sdate = _mdl.sdate + " " + _mdl.stime;
            string _edate = _mdl.sdate + " " + _mdl.etime;
            TimeSpan span = (Convert.ToDateTime(_edate) - Convert.ToDateTime(_sdate));
            var estimhour = String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes);
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@tecrate", tecrate);
                _srt.Add("@clientrate", clientrate);
                _srt.Add("@MapValidateAddress", MapValidateAddress);
                _srt.Add("@address", _mdl.street);
                _srt.Add("@cityId", _mdl.city_id);
                _srt.Add("@stateId", _mdl.state_id);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@projectId", projectId);
                _srt.Add("@JobId", _mdl.JobId);
                _srt.Add("@Ttile", _mdl.title);
                _srt.Add("@Client", _mdl.client_id);
                _srt.Add("@Technician", _mdl.Technician);
                _srt.Add("@Dispatcher", _mdl.Dispatcher);
                _srt.Add("@Project_manager", _mdl.mgr_id);//client manager
                _srt.Add("@Project_manager1", _mdl.mgr_id1);
                _srt.Add("@startdate", _sdate);
                _srt.Add("@expense", _mdl.Expense);
                _srt.Add("@enddate", _edate);
                _srt.Add("@est_Hours", estimhour);
                _srt.Add("@Status_id", _mdl.Status);
                _srt.Add("@suite", _mdl.suite);
                _srt.Add("@Description", _mdl.description);
                _srt.Add("@createdby", userid);
                _srt.Add("@Contact", _mdl.Contact);
                _srt.Add("@Phone", _mdl.Phone);
                message = sqlHelper.executeNonQueryWMessage("AddNewJob", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public bool validateJobId(string JobId, int workspaceid)
        {
            bool isavailable = true;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", JobId);
                _srt.Add("@workspaceid", workspaceid);
                isavailable = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("validate_jobid", "", _srt).ToString()) > 0 ? false : true;
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return isavailable;
        }
        public bool validateJobId(int Id, string jobid, int workspaceid)
        {
            bool isavailable = true;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", Id);
                _srt.Add("@jobid", jobid);
                _srt.Add("@workspaceid", workspaceid);
                isavailable = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("validate_jobid_editjob", "", _srt).ToString()) > 0 ? false : true;
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return isavailable;
        }
        public void update_status(int statusId, int jobid, int userId, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@statusId", statusId);
                _srt.Add("@jobid", jobid);
                sqlHelper.executeNonQuery("tbl_job_update_status", "", _srt);
                if (statusId == 5)
                {
                    job _mdl = GetAllActiveJobDetailsById(jobid);
                    attandanceadmin_items client = new attandanceadmin_items();
                    client.startdate = _mdl.sdate;
                    client.starttime = _mdl.stime;
                    client.endtime = _mdl.etime;
                    client._memberrate = _mdl.Pay_rate;
                    client.expnse = _mdl.Expense;
                    client.memberid = _mdl.Technician;
                    client.jobid = _mdl._jobid;
                    client.comment = "Pay Created By Approved Job";
                    new attandanceservices().add_new_pay(client, userId, workspaceid: workspaceid);
                }
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //return isavailable;
        }
        public string UpdateJob(job _mdl, int userid, int workspaceid, int projectId)
        {
            string message = "Job Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string _sdate = _mdl.sdate + " " + _mdl.stime;
            string _edate = _mdl.sdate + " " + _mdl.etime;
            TimeSpan span = (Convert.ToDateTime(_edate) - Convert.ToDateTime(_sdate));
            var estimhour = Convert.ToDouble(String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes));
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@tecrate", _mdl.Pay_rate);
                _srt.Add("@clientrate", _mdl.Client_rate);
                _srt.Add("@address", _mdl.address);
                _srt.Add("@MapValidateAddress", _mdl.MapValidateAddress);
                _srt.Add("@cityId", _mdl.city_id);
                _srt.Add("@StateId", _mdl.state_id);
                _srt.Add("@jobid", _mdl._jobid);
                _srt.Add("@JobtitleId", _mdl.JobId);
                _srt.Add("@projectId", projectId);
                _srt.Add("@Ttile", _mdl.title);
                _srt.Add("@Client", _mdl.client_id);
                _srt.Add("@Technician", _mdl.Technician);
                _srt.Add("@Dispatcher", _mdl.Dispatcher);
                _srt.Add("@Project_manager", _mdl.mgr_id);//client manager
                _srt.Add("@Project_manager1", _mdl.mgr_id1);
                _srt.Add("@startdate", _sdate);
                _srt.Add("@suite", _mdl.suite);
                _srt.Add("@expense", _mdl.Expense);
                _srt.Add("@enddate", _edate);
                _srt.Add("@est_Hours", estimhour);
                _srt.Add("@Status_id", _mdl.Status);
                _srt.Add("@Description", _mdl.description);
                _srt.Add("@createdby", userid);
                _srt.Add("@clr1", _mdl.clr1);
                _srt.Add("@clr2", _mdl.clr2);
                _srt.Add("@Contact", _mdl.Contact);
                _srt.Add("@Phone", _mdl.Phone);
                _srt.Add("@Instruction", _mdl.Instruction);
                sqlHelper.executeNonQuery("UpdateJob", "", _srt);
                if (_mdl.Status == 5)
                {
                    attandanceadmin_items client = new attandanceadmin_items();
                    client.startdate = _mdl.sdate;
                    client.starttime = _mdl.stime;
                    client.endtime = _mdl.etime;
                    client._memberrate = _mdl.Pay_rate;
                    client.expnse = _mdl.Expense;
                    client.memberid = _mdl.Technician;
                    client.jobid = _mdl._jobid;
                    client.comment = "Pay Created By Approved Job";
                    new attandanceservices().add_new_pay(client, userid, workspaceid: workspaceid);
                }
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_job_byview(job_items1 _mdl, int userid, int projectId)
        {
            string message = "Job Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();

            try
            {
                SortedList _srt = new SortedList();
                //_srt.Add("@fname", _mdl.FirstName);
                _srt.Add("@JobId", _mdl.JobID);
                _srt.Add("@Client_id", _mdl.Client_id);
                _srt.Add("@Client_rate", _mdl.Client_rate);
                _srt.Add("@Technicianid", _mdl.Technicianid);
                _srt.Add("@pay_rate", _mdl.pay_rate);
                _srt.Add("@mgr_id", _mdl.mgr_id);
                _srt.Add("@mgr_id1", _mdl.mgr_id1);
                _srt.Add("@dispatcher", _mdl.Dispatcherid);
                _srt.Add("@expense", _mdl.Expense);
                _srt.Add("@Status_id", _mdl.Status); _srt.Add("@projectid", projectId);
                sqlHelper.executeNonQuery("tbl_job_updatenewjob", "", _srt);
                if (_mdl.Status == 5)
                {
                    attandanceadmin_items client = new attandanceadmin_items();
                    client.startdate = _mdl.sdate; client.starttime = _mdl.stime;
                    client.endtime = _mdl.etime;
                    client._memberrate = _mdl.pay_rate;
                    client.expnse = _mdl.Expense;
                    client.memberid = _mdl.Technicianid;
                    client.jobid = _mdl.JobID;
                    client.comment = "Pay Created By Approved Job";
                    new attandanceservices().add_new_pay(client, userid);
                }

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_job_byviewT(job_items1 _mdl, int userid)
        {
            string message = "Job Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();

            try
            {
                SortedList _srt = new SortedList();
                //_srt.Add("@fname", _mdl.FirstName);
                _srt.Add("@JobId", _mdl.JobID);
                _srt.Add("@dispatcher", _mdl.Dispatcherid);
                _srt.Add("@rate", _mdl.pay_rate);
                _srt.Add("@expense", _mdl.Expense);
                _srt.Add("@Status_id", _mdl.Status);
                sqlHelper.executeNonQuery("tbl_job_updatenewjobByT", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string add_new_comment(string comment, int userid, int jobid)
        {
            string message = "Comment Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            var cst = DateTime.UtcNow.AddHours(-6);
            try
            {
                SortedList _srt = new SortedList();
                //_srt.Add("@fname", _mdl.FirstName);
                _srt.Add("@JobId", jobid);
                _srt.Add("@comment", comment);
                _srt.Add("@createdby", userid);
                _srt.Add("@createddate", cst);
                sqlHelper.executeNonQuery("tbl_notification_tbl_job_comment_add", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string delete_job(int jobid)
        {
            string message = "Job Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();

            try
            {
                SortedList _srt = new SortedList();
                //_srt.Add("@fname", _mdl.FirstName);
                _srt.Add("@JobId", jobid);
                sqlHelper.executeNonQuery("tbl_job__remove_v2", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string AddNewDocument(string FileTitle, int userid, int jobid, string Size, string url, int parentid = 0)
        {
            string message = "File Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            var cst = DateTime.UtcNow.AddHours(-6);

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@createddate", cst);
                _srt.Add("@FileTile", FileTitle);
                _srt.Add("@Jobid", jobid);
                _srt.Add("@Size", Size);
                _srt.Add("@url", url);
                _srt.Add("@createdby", userid);
                if (parentid != 0)
                {
                    _srt.Add("@parentid", parentid);
                }
                sqlHelper.executeNonQuery("tbl_jobfiles_add", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string delete_doc(int id)
        {
            string message = "File Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                sqlHelper.executeNonQuery("tbl_jobfiles_remove", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string delete_docByParent(int id)
        {
            string message = "File Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                sqlHelper.executeNonQuery("tbl_jobfiles_removeByParent", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string assignjob(int Jobid, int memberid, string rate)
        {
            string message = "File Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", Jobid);
                _srt.Add("@memberid", memberid);
                _srt.Add("@payrate", rate);
                message = sqlHelper.executeNonQueryWMessage("assignjobtonewmember", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public jobmodal GetAllActiveJobsForCalender(string date = "", string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int workspaceid = 0, int project = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            jobmodal _mdl = new jobmodal();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                _srt.Add("@workspaceid", workspaceid);
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (project != 0)
                {
                    _srt.Add("@project", project);
                }

                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                string sp = "GetAllActiveJobsByUserIdForCalender";
                if (Status != 0)
                {
                    // sp = "tbl_job_getalljobs_index_withstatus";
                    _srt.Add("@Status", Status);
                }
                var dt = sqlHelper.fillDataTable(sp, "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items();
                    _item.Job_ID = dt.Rows[i]["JobId"].ToString();
                    _item.cityname = dt.Rows[i]["city"].ToString();
                    _item.statename = dt.Rows[i]["state"].ToString();
                    _item.Title = dt.Rows[i]["Ttile"].ToString();
                    _item.Client = dt.Rows[i]["Client"].ToString().ToTitleCase();
                    _item.Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase();
                    _item.Dispatcher = dt.Rows[i]["Dispatcher"].ToString().ToTitleCase();
                    _item.Project_manager = dt.Rows[i]["Project_manager"].ToString().ToTitleCase();
                    _item.sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Client_rate = dt.Rows[i]["Client_rate"].ToString().Currency();
                    _item._sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString());
                    _item._endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString());
                    _item.Pay_rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.Est_hours = dt.Rows[i]["est_Hours"].ToString();
                    _item.hours = dt.Rows[i]["hours"].ToString();
                    _item.Expense = dt.Rows[i]["Expnese"].ToString().Currency();
                    _item.Status = dt.Rows[i]["Status_id"].ToString();
                    _item.JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    // _item.status_id = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString());
                    //total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._jobs = _jbs;
            return _mdl;
        }
        public List<job_items> GetAllActiveJobsByIndex(ref int total, int startindex, int endindex, string search = "", string date = "", string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int userid = 0, int notificationUSER = 0, string stype = "T", int workspaceid = 0, int projectId = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            jobmodal _mdl = new jobmodal();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                _srt.Add("@startindex", startindex);
                _srt.Add("@endindex", endindex);
                _srt.Add("@workspaceid", workspaceid);
                if (userid != 0)
                {
                    _srt.Add("@id", userid);
                    stype = new UserService().GetUserType(userid);
                }
                if (projectId != 0)
                {
                    _srt.Add("@projectId", projectId);
                }
                if (stype == "S" || stype == "LT")
                {
                    _srt.Add("@companyid", userid);
                }
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                string sp = "GetAllActiveJobsByIndexWithoutStatus";
                if (Status != 0)
                {
                    sp = "GetAllActiveJobsByIndexWithStatus";
                    _srt.Add("@Status", Status);
                }
                var dt = sqlHelper.fillDataTable(sp, "", _srt);
                notificationServices _nser = new notificationServices();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items();
                    _item.Job_ID = dt.Rows[i]["JobId"].ToString().Count() > 8 ? dt.Rows[i]["JobId"].ToString().Substring(0, 8) : dt.Rows[i]["JobId"].ToString();
                    _item.cityname = dt.Rows[i]["city"].ToString();
                    _item.statename = dt.Rows[i]["state"].ToString();
                    _item.Title = dt.Rows[i]["Ttile"].ToString().Count() > 30 ? dt.Rows[i]["Ttile"].ToString().Substring(0, 30) : dt.Rows[i]["Ttile"].ToString();
                    _item.Client = dt.Rows[i]["Client"].ToString();
                    _item.Technician = dt.Rows[i]["Technician"].ToString();
                    _item.Dispatcher = dt.Rows[i]["Dispatcher"].ToString();
                    _item.Project_manager = dt.Rows[i]["Project_manager"].ToString();
                    _item.sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Client_rate = stype == "ST" ? "XXXX" : dt.Rows[i]["Client_rate"].ToString().Currency();
                    _item._sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString());
                    _item._endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString());
                    _item.Pay_rate = stype == "ST" ? "XXXX" : dt.Rows[i]["rate"].ToString().Currency();
                    _item.Est_hours = dt.Rows[i]["est_Hours"].ToString();
                    _item.hours = dt.Rows[i]["hours"].ToString();
                    _item.Expense = dt.Rows[i]["Expnese"].ToString().Currency();
                    _item.Status = dt.Rows[i]["Status"].ToString();
                    _item.JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _item.status_id = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString());
                    _item.commentCount = _nser.GetNotificationCountByType(_item.JobID, NotificationType.Job, notificationUSER);
                    _item.confirm = Convert.ToInt32(dt.Rows[i]["Confirmationtype"].ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //_mdl._jobs = _jbs;
            return _jbs;
        }
        public jobmodal GetAllActiveJobsByUserIdForCalender(int id, string date, string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int userid = 0, string membertype = "T", int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            jobmodal _mdl = new jobmodal();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (userid != 0)
                {
                    _srt.Add("@userid", userid);
                }
                _srt.Add("@workspaceid", workspaceid);

                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                if (Status != 0)
                {
                    _srt.Add("@Status", Status);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                _srt.Add("@id", id);
                var dt = sqlHelper.fillDataTable("GetAllActiveJobsByUserIdForCalender", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items();
                    _item.Job_ID = dt.Rows[i]["JobId"].ToString();
                    _item.cityname = dt.Rows[i]["city"].ToString();
                    _item.statename = dt.Rows[i]["state"].ToString();
                    _item.Title = dt.Rows[i]["Ttile"].ToString();
                    _item.Client = dt.Rows[i]["Client"].ToString().ToTitleCase();
                    _item.Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase();
                    _item.Dispatcher = dt.Rows[i]["Dispatcher"].ToString().ToTitleCase();
                    _item.Project_manager = dt.Rows[i]["Project_manager"].ToString().ToTitleCase();
                    _item.sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd hh:mm tt");
                    _item.endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("yyyy-MM-dd hh:mm tt");
                    _item.Client_rate = dt.Rows[i]["Client_rate"].ToString().Currency();
                    _item.Pay_rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.Est_hours = dt.Rows[i]["est_Hours"].ToString();
                    _item.hours = dt.Rows[i]["hours"].ToString();
                    _item.Expense = dt.Rows[i]["Expnese"].ToString().Currency();
                    _item.Status = dt.Rows[i]["Status_id"].ToString();
                    _item.JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item._sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString());
                    _item._endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString());

                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._jobs = _jbs;
            return _mdl;
        }
        public job_items1 GetAllActiveJobById(int id, int NotificationUser, int _workspaceId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            job_items1 _item = new job_items1();
            //SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            //List<job_items> _jbs = new List<job_items>();
            List<commentmodal> _commnts = new List<commentmodal>();
            List<FileModel> _files = new List<FileModel>();
            notificationServices _nser = new notificationServices();
            List<int> ReminderIds = new List<int>();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobid", id);
                var ds = sqlHelper.fillDataSet("GetAllActiveJobById", "", _srt);
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.Job_ID = dt.Rows[0]["JobId"].ToString();
                    _item.Title = dt.Rows[0]["Ttile"].ToString();
                    _item.Client = dt.Rows[0]["Client"].ToString();
                    _item.MapValidateAddress = dt.Rows[0]["address"].ToString().GetAddressView(dt.Rows[0]["SuiteNumber"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[0]["id"].ToString());
                    _item.distoken = _crypt.EncryptStringAES(dt.Rows[0]["Dispatcherid"].ToString());
                    _item.tectoken = _crypt.EncryptStringAES(dt.Rows[0]["Technicianid"].ToString());
                    _item.Client_id = Convert.ToInt32(dt.Rows[0]["Client"].ToString());
                    _item.projectId = Convert.ToInt32(dt.Rows[0]["Projectid"].ToString());
                    _item.mgr_id = Convert.ToInt32(dt.Rows[0]["Project_manager"].ToString());
                    _item.mgr_id1 = Convert.ToInt32(dt.Rows[0]["Project_manager1"].ToString());
                    _item.Technician = dt.Rows[0]["Technician"].ToString();
                    _item.Dispatcher = dt.Rows[0]["Dispatcher"].ToString();
                    _item.Project_manager = dt.Rows[0]["Project_manager"].ToString();
                    _item.sdate = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Client_rate = dt.Rows[0]["Client_rate"].ToString().Currency();
                    _item.Expense = dt.Rows[0]["expense"].ToString().Currency();
                    _item.Est_hours = dt.Rows[0]["est_Hours"].ToString();
                    _item.Status = Convert.ToInt32(dt.Rows[0]["Status_id"].ToString());
                    _item.JobID = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    _item.description = dt.Rows[0]["description"].ToString();
                    _item.pay_rate = dt.Rows[0]["pay_rate"].ToString().Currency();
                    _item.Technicianid = Convert.ToInt32(dt.Rows[0]["Technicianid"].ToString());
                    _item.Dispatcherid = Convert.ToInt32(dt.Rows[0]["Dispatcherid"].ToString());
                    _item.unreadcount = _nser.GetNotificationCountByType(_item.JobID, NotificationType.Job, NotificationUser);
                    _item.Instruction = dt.Rows[0]["Instruction"].ToString();
                    _item.Contact = dt.Rows[0]["Contact"].ToString();
                    _item.Phone = dt.Rows[0]["Phone"].ToString();
                }
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    commentmodal _mdl = new commentmodal();
                    _mdl.Comment = dt1.Rows[i]["Comment"].ToString();
                    _mdl.Commentid = Convert.ToInt32(dt1.Rows[i]["Id"].ToString());
                    _mdl.createdby = dt1.Rows[i]["createdby"].ToString();
                    _mdl.CreatedDate = dt1.Rows[i]["CreatedDate"].ToString();
                    _mdl.isread = dt1.Rows[i]["readusers"].ToString().Contains(NotificationUser.ToString());
                    _mdl.createdbyId = Convert.ToInt32(dt1.Rows[i]["crtedby"].ToString());//crtedby
                    if (_mdl.createdbyId == NotificationUser)
                    {
                        _mdl.isread = true;
                    }
                    _commnts.Add(_mdl);
                }
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    FileModel _mdl = new FileModel();
                    _mdl.FileTitle = dt2.Rows[i]["Filetile"].ToString();
                    _mdl.FileId = Convert.ToInt32(dt2.Rows[i]["id"].ToString());
                    _mdl.Size = dt2.Rows[i]["Size"].ToString();
                    _mdl.Url = dt2.Rows[i]["url"].ToString();
                    _mdl.CreatedBy = dt2.Rows[i]["createdby"].ToString();
                    _mdl.CreatedDate = dt2.Rows[i]["Createddate"].ToString();
                    _mdl.FileToken = _crypt.EncryptStringAES(dt2.Rows[i]["id"].ToString());
                    _files.Add(_mdl);
                }
                _item._Comments = _commnts;
                _item._files = _files;
                ProcessReminders(id, _item.projectId, _workspaceId);
                var reminders = GetReminderSettingForJob(ref ReminderIds, id);
                _item.techreminder = reminders.techReminder;
                List<int> Tools = new List<int>();
                List<int> Skiils = new List<int>();
                _item.dispatcherreminder = reminders.dispatcherReminder;
                GetToolsSkillsByJobId(id, ref Tools, ref Skiils);
                _item.tools = Tools.ToArray();
                _item.skills = Skiils.ToArray();
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _item;
        }

        public List<string> GetFilesUrlbyJobId(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<String> _files = new List<String>();
            try
            {
                string path = "";
                SortedList _srt = new SortedList();
                _srt.Add("@jobid", id);
                var dt = sqlHelper.fillDataTable("GetFilesUrlbyJobId", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    path = HttpContext.Current.Server.MapPath("~" + dt.Rows[i]["url"].ToString());
                    _files.Add(path);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();

            }
            finally
            {
                sqlHelper.Dispose();

            }
            return _files;
        }

        public void GetToolsSkillsByJobId(int _jobId, ref List<int> Tools, ref List<int> Skiils)
        {

            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobId", _jobId);
                var ds = sqlHelper.fillDataSet("GetToolsSkillsByJobId", "", _srt);
                var toolsdt = ds.Tables[0];
                var skillsdt1 = ds.Tables[1];
                if (toolsdt.Rows.Count > 0)
                {
                    for (int i = 0; i < toolsdt.Rows.Count; i++)
                    {
                        Tools.Add(Convert.ToInt32(toolsdt.Rows[i][0].ToString()));
                    }

                }

                if (skillsdt1.Rows.Count > 0)
                {
                    for (int i = 0; i < skillsdt1.Rows.Count; i++)
                    {
                        Skiils.Add(Convert.ToInt32(skillsdt1.Rows[i][0].ToString()));
                    }

                }

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();

            }
            finally
            {
                sqlHelper.Dispose();

            }

        }


        public List<string> GetToolsNameByJobId(int _jobId)
        {
            List<string> ToolsName = new List<string>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobId", _jobId);
                var toolsdt = sqlHelper.fillDataTable("GetToolsNameByJobId", "", _srt);
                if (toolsdt.Rows.Count > 0)
                {
                    for (int i = 0; i < toolsdt.Rows.Count; i++)
                    {
                        ToolsName.Add(toolsdt.Rows[i][0].ToString());
                    }

                }
            }
            catch (Exception exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return ToolsName;
        }

        public void ProcessReminders(int _jobId, int _projectId, int _workSpaceId)
        {
            List<int> _reminderIds = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            Crypto _crypt = new Crypto();
            List<int> ProcessReminderIds = new List<int>();
            int Id = 0;
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@projectId", _projectId);
                _srt.Add("@workSpaceId", _workSpaceId);
                _srt.Add("@jobId", _jobId);
                var ds = sqlHelper.fillDataSet("ProcessReminders", "", _srt);
                var dt0 = ds.Tables[0];
                var dt = ds.Tables[1];
                ds.Dispose();

                for (int i = 0; i < dt0.Rows.Count; i++)
                {
                    Id = Convert.ToInt32(dt0.Rows[i]["parentid"].ToString());
                    if (!_reminderIds.Contains(Id)) { _reminderIds.Add(Id); }
                }
                ///parentid
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Id = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    if (!_reminderIds.Contains(Id)) { ProcessReminderIds.Add(Id); }
                }
                dt.Dispose();
                if (ProcessReminderIds.Count > 0)
                {
                    _srt.Clear();
                    _srt.Add("@jobId", _jobId);
                    _srt.Add("@ProcessReminderIds", ProcessReminderIds.ToArray().ToDelimitedString());
                    sqlHelper.executeNonQuery("ProcessedReminders", "", _srt);
                }
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();

            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._jobs = _jbs;

        }

        public job GetAllActiveJobDetailsById(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Crypto _crypt = new Crypto();
            job _item = new job();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobid", id);
                var dt = sqlHelper.fillDataTable("GetAllActiveJobDetailsById", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.JobId = dt.Rows[0]["JobId"].ToString();
                    _item.title = dt.Rows[0]["Ttile"].ToString();
                    _item.client_id = Convert.ToInt32(dt.Rows[0]["Client"].ToString());
                    _item._projectid = Convert.ToInt32(dt.Rows[0]["projectId"].ToString());
                    _item.mgr_id = Convert.ToInt32(dt.Rows[0]["Project_manager"].ToString());
                    _item.mgr_id1 = Convert.ToInt32(dt.Rows[0]["Project_manager1"].ToString());
                    _item.sdate = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.edate = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Est_hours = Convert.ToDouble(dt.Rows[0]["est_Hours"].ToString());
                    _item.Status = Convert.ToInt32(dt.Rows[0]["Status_id"].ToString());
                    _item._jobid = id;
                    _item.token = _crypt.EncryptStringAES(id.ToString());
                    _item.description = dt.Rows[0]["Description"].ToString();
                    _item.Technician = Convert.ToInt32(dt.Rows[0]["Technician"].ToString());
                    _item.Dispatcher = Convert.ToInt32(dt.Rows[0]["Dispatcher"].ToString());
                    _item.Expense = dt.Rows[0]["Expnese"].ToString().Currency();
                    _item.Client_rate = dt.Rows[0]["Client_rate"].ToString().Currency();
                    _item.Pay_rate = dt.Rows[0]["payrate"].ToString().Currency();
                    _item.MapValidateAddress = dt.Rows[0]["address"].ToString();
                    _item.suite = dt.Rows[0]["SuiteNumber"].ToString();
                    _item.Contact = dt.Rows[0]["Contact"].ToString();
                    _item.Phone = dt.Rows[0]["Phone"].ToString();
                    _item.Instruction = dt.Rows[0]["Instruction"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._jobs = _jbs;
            return _item;
        }



        public remindersetting GetReminderSettingForJob(ref List<int> _reminderIds, int jobId = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            remindersetting _mdl = new remindersetting();
            List<reminder> techreminder = new List<reminder>();
            List<reminder> dispatcherreminder = new List<reminder>();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobId", jobId);
                var ds = sqlHelper.fillDataSet("GetReminderSettingForJob", "", _srt);
                var dt6 = ds.Tables[0];
                var dt7 = ds.Tables[1];
                if (dt6.Rows.Count > 0)
                {
                    for (int i = 0; i < dt6.Rows.Count; i++)
                    {
                        reminder techreminder1 = new reminder();
                        techreminder1.Projectid = Convert.ToInt32(dt6.Rows[i]["Projectid"].ToString());
                        techreminder1.token = new Crypto().EncryptStringAES(dt6.Rows[i]["id"].ToString());
                        techreminder1._reminderId = Convert.ToInt32(dt6.Rows[i]["id"].ToString());
                        techreminder1.repeatid = Convert.ToInt32(dt6.Rows[i]["repeatid"].ToString());
                        techreminder1.reapetdays = Convert.ToInt32(dt6.Rows[i]["reapetdays"].ToString());
                        techreminder1.repeattype = Convert.ToInt32(dt6.Rows[i]["ReminderType"].ToString());
                        techreminder1.Attachment = dt6.Rows[i]["Attachment"].ToString().StringToArray(",");
                        techreminder1.info = dt6.Rows[i]["info"].ToString().StringToArray(",");
                        techreminder1.type = dt6.Rows[i]["type"].ToString().StringToArray(",");
                        techreminder1.Confirmation = Convert.ToInt32(dt6.Rows[i]["Confirmation"].ToString());
                        _reminderIds.Add(Convert.ToInt32(dt6.Rows[i]["parentid"].ToString()));
                        techreminder.Add(techreminder1);
                    }

                }
                _mdl.techReminder = techreminder;
                if (dt7.Rows.Count > 0)
                {
                    for (int i = 0; i < dt7.Rows.Count; i++)
                    {
                        reminder dispatcherreminder1 = new reminder();
                        dispatcherreminder1.Projectid = Convert.ToInt32(dt7.Rows[i]["Projectid"].ToString());
                        dispatcherreminder1.token = new Crypto().EncryptStringAES(dt7.Rows[i]["id"].ToString());
                        dispatcherreminder1._reminderId = Convert.ToInt32(dt7.Rows[i]["id"].ToString());
                        dispatcherreminder1.repeatid = Convert.ToInt32(dt7.Rows[i]["repeatid"].ToString());
                        dispatcherreminder1.reapetdays = Convert.ToInt32(dt7.Rows[i]["reapetdays"].ToString());
                        dispatcherreminder1.repeattype = Convert.ToInt32(dt7.Rows[i]["ReminderType"].ToString());
                        dispatcherreminder1.Attachment = dt7.Rows[i]["Attachment"].ToString().StringToArray(",");
                        dispatcherreminder1.info = dt7.Rows[i]["info"].ToString().StringToArray(",");
                        dispatcherreminder1.type = dt7.Rows[i]["type"].ToString().StringToArray(",");
                        dispatcherreminder1.Confirmation = Convert.ToInt32(dt7.Rows[i]["Confirmation"].ToString());
                        _reminderIds.Add(Convert.ToInt32(dt7.Rows[i]["parentid"].ToString()));
                        dispatcherreminder.Add(dispatcherreminder1);
                    }

                }
                _mdl.dispatcherReminder = dispatcherreminder;
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _mdl;
        }


    }
}
