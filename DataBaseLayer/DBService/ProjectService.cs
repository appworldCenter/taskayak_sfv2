﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{
    public class projectServices
    {
        public string GetLatestProjectId(int _workspaceId)
        {
            string ProjectId = "0001";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList();
                _SortedList.Add("@workspaceId", _workspaceId);
                ProjectId = _SqlHelper.executeNonQueryWMessage("GetLatestProjectId", "", _SortedList).ToString();
                ProjectId = ProcessProjectId4Digit(ProjectId);
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                _SqlHelper.Dispose();
            }
            return ProjectId;
        }

        public string ProcessProjectId4Digit(string _projectId)
        {
            int Length = _projectId.Length;
            switch (Length)
            {
                case 1:
                    _projectId = "000" + _projectId;
                    break;
                case 2:
                    _projectId = "00" + _projectId;
                    break;
                case 3:
                    _projectId = "0" + _projectId;
                    break;

            }
            return _projectId;
        }

        public bool validateProjectId(string ProjectId, int workspaceid)
        {
            bool isavailable = true;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", ProjectId);
                _srt.Add("@workspaceid", workspaceid);
                isavailable = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("validate_projectid", "", _srt).ToString()) > 0 ? false : true;
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return isavailable;
        }

        public _projectdetails GetProjectdetails(int projectId)
        {
            _projectdetails _model = new _projectdetails();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", projectId);
                var dt = sqlHelper.fillDataTable("GetProjectDetailsById", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _model.clientId = Convert.ToInt32(dt.Rows[0]["clientId"].ToString());
                    _model.ClientManagerId = Convert.ToInt32(dt.Rows[0]["ClientManagerId"].ToString());
                    _model.projectManagerId = Convert.ToInt32(dt.Rows[0]["ProjectManagerId"].ToString());
                }
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return _model;
        }

        public void deleteproject(int ProjectId)
        {

            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", ProjectId);
                sqlHelper.executeNonQuery("removeProject", "", _srt);
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

        }

        public void update_status(int ProjectId, int statusid)
        {

            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", ProjectId);
                _srt.Add("@statusid", statusid);
                sqlHelper.executeNonQuery("updateProject", "", _srt);
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

        }

        public string add_new_Project(ProjectModal _mdl, int workspaceid)
        {
            string message = "Project Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string skills = "";
            string tools = "";
            string screening = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@ID", _mdl.ProjectId);
                _srt.Add("@clientId", _mdl.clientId);
                _srt.Add("@clientManagerId", _mdl.clientManagerId);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@description", _mdl.description);
                _srt.Add("@specialInstruction", _mdl.specialInstruction);
                _srt.Add("@email", _mdl.email);
                _srt.Add("@projectManagerId", _mdl.projectManagerId);
                _srt.Add("@status", _mdl.status);
                _srt.Add("@title", _mdl.title);
                if (_mdl.skills != null)
                {
                    skills = string.Join<int>(",", _mdl.skills);
                    _srt.Add("@skills", skills);
                }
                if (_mdl.tools != null)
                {
                    tools = string.Join<int>(",", _mdl.tools);
                    _srt.Add("@tools", tools);
                }
                if (_mdl.Screening != null)
                {
                    screening = string.Join<int>(",", _mdl.Screening);
                    _srt.Add("@screening", screening);
                }
                sqlHelper.executeNonQuery("AddnewProject", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public List<_projectitems> get_all_active_jobs_by_index(ref int total, int startindex, int endindex, string search = "", string date = "", int cclient = 0, int cmanager = 0, int pmanager = 0, int Status = 0, int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            _projectmodal _mdl = new _projectmodal();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<_projectitems> _jbs = new List<_projectitems>();
            int jobcount = 0;
            int offercount = 0;
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                _srt.Add("@startindex", startindex);
                _srt.Add("@endindex", endindex);
                _srt.Add("@workspaceid", workspaceid);
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cmanager != 0)
                {
                    _srt.Add("@cmanager", cmanager);
                }
                if (pmanager != 0)
                {
                    _srt.Add("@pmanager", pmanager);
                }
                string sp = "tbl_projects_getallproject_index_withnostatus_v2";
                if (Status != 0)
                {
                    sp = "tbl_projects_getallproject_index_withstatus_v2";
                    _srt.Add("@Status", Status);
                }
                var dt = sqlHelper.fillDataTable(sp, "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    jobcount = 0;
                    offercount = 0;
                    _projectitems _item = new _projectitems();
                    _item.IsDefaultProject = Convert.ToInt32(dt.Rows[i]["Isdefault"].ToString());//
                    _item.project_ID = dt.Rows[i]["IndexId"].ToString().Count() > 8 ? dt.Rows[i]["IndexId"].ToString().Substring(0, 8) : dt.Rows[i]["IndexId"].ToString();
                    _item.Title = dt.Rows[i]["ProjectName"].ToString().Count() > 30 ? dt.Rows[i]["ProjectName"].ToString().Substring(0, 30) : dt.Rows[i]["ProjectName"].ToString();
                    _item.Client = dt.Rows[i]["Client"].ToString().ToTitleCase();
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["ProjectId"].ToString());
                    _item.Status = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString()) == 1 ? "Open" : "Close";
                    _item.status_id = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString());
                    _item.ID = Convert.ToInt32(dt.Rows[i]["ProjectId"].ToString());
                    _item.clientmanager = dt.Rows[i]["ClientManager"].ToString().ToTitleCase();
                    _item.Project_manager = dt.Rows[i]["ProjectManager"].ToString().ToTitleCase();
                    _item.reminderCount = GetReminderJobCount(Convert.ToInt32(dt.Rows[i]["ProjectId"].ToString()));
                    GetofferJobCount(Convert.ToInt32(dt.Rows[i]["ProjectId"].ToString()), workspaceid, ref offercount, ref jobcount);
                    _item.offerCount = offercount;
                    _item.jobCount = jobcount;
                    _item.commentCount = 0;
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //_mdl._jobs = _jbs;
            return _jbs;
        }

        public void GetofferJobCount(int projectid, int workspaceId, ref int offercount, ref int jobcount)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@projectid", projectid);
                _srt.Add("@workspaceId", workspaceId);
                var dt = sqlHelper.fillDataTable("GetjobOffercount", "", _srt);
                offercount = Convert.ToInt32(dt.Rows[0][0].ToString());
                jobcount = Convert.ToInt32(dt.Rows[0][1].ToString());
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
        }


        public int GetReminderJobCount(int projectId)
        {
            int remindercount = 0;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", projectId);
                var dt = sqlHelper.fillDataTable("getReminderCount", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    remindercount = Convert.ToInt32(dt.Rows[0][0].ToString());
                }

            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return remindercount;
        }
        public string update_Project(ProjectModal _mdl, int ProjectId)
        {
            string message = "Project Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string skills = "";
            string tools = "";
            string screening = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@ID", ProjectId);
                _srt.Add("@clientId", _mdl.clientId);
                _srt.Add("@clientManagerId", _mdl.clientManagerId);
                _srt.Add("@email", _mdl.email);
                _srt.Add("@projectManagerId", _mdl.projectManagerId);
                _srt.Add("@status", _mdl.status);
                _srt.Add("@title", _mdl.title);
                _srt.Add("@description", _mdl.description);
                _srt.Add("@specialInstruction", _mdl.specialInstruction);
                if (_mdl.skills != null)
                {
                    skills = string.Join<int>(",", _mdl.skills);
                    _srt.Add("@skills", skills);
                }
                if (_mdl.tools != null)
                {
                    tools = string.Join<int>(",", _mdl.tools);
                    _srt.Add("@tools", tools);
                }
                if (_mdl.Screening != null)
                {
                    screening = string.Join<int>(",", _mdl.Screening);
                    _srt.Add("@screening", screening);
                }
                sqlHelper.executeNonQuery("UpdateProjectDetails_v2", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_new_Project(ProjectModal _mdl, int ProjectId)
        {
            string message = "Project Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string screening = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@ID", ProjectId);
                _srt.Add("@clientId", _mdl.clientId);
                _srt.Add("@clientManagerId", _mdl.clientManagerId);
                _srt.Add("@email", _mdl.email);
                _srt.Add("@projectManagerId", _mdl.projectManagerId);
                _srt.Add("@status", _mdl.status);
                _srt.Add("@title", _mdl.title);
                if (_mdl.Screening != null)
                {
                    screening = string.Join<int>(",", _mdl.Screening);
                    _srt.Add("@screening", screening);
                }
                sqlHelper.executeNonQuery("UpdateProjectDetails", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update_skills(int[] skills, int ProjectId)
        {
            string message = "Skills Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string _skills = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@ID", ProjectId);
                if (skills != null)
                {
                    _skills = string.Join<int>(",", skills);
                    _srt.Add("@skills", _skills);
                }
                sqlHelper.executeNonQuery("UpdateProjectskilldDetails_v2", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string UpdateSkillsByJob(int[] skill, int _jobId)
        {
            string message = "Skills Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string _skills;
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobId", _jobId);
                if (skill != null)
                {
                    _skills = string.Join<int>(",", skill);
                    _srt.Add("@skills", _skills);
                }
                sqlHelper.executeNonQuery("UpdateJobkilldDetails", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string UpdateSkillsByOfferId(int[] skill, int _OfferId)
        {
            string message = "Skills Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string _skills;
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@OfferId", _OfferId);
                if (skill != null)
                {
                    _skills = string.Join<int>(",", skill);
                    _srt.Add("@skills", _skills);
                }
                sqlHelper.executeNonQuery("UpdateOfferskillsDetails", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string UpdateToolsByJob(int[] tools, int _jobId)
        {
            string message = "Tools Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string _skills = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobId", _jobId);
                if (tools != null)
                {
                    _skills = string.Join<int>(",", tools);
                    _srt.Add("@tools", _skills);
                }
                sqlHelper.executeNonQuery("UpdateJobtoolsDetails", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string UpdateToolsByOffer(int[] tools, int _offerId)
        {
            string message = "Tools Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string _skills = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@offerId", _offerId);
                if (tools != null)
                {
                    _skills = string.Join<int>(",", tools);
                    _srt.Add("@tools", _skills);
                }
                sqlHelper.executeNonQuery("UpdateoffertoolsDetails", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update_tools(int[] tools, int ProjectId)
        {
            string message = "Tools Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string _skills = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@ID", ProjectId);
                if (tools != null)
                {
                    _skills = string.Join<int>(",", tools);
                    _srt.Add("@tools", _skills);
                }
                sqlHelper.executeNonQuery("UpdateProjecttoolsDetails_v2", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string addprojectComment(string Comment, int ProjectId, int userid, ref List<int> jobs, ref List<int> offers)
        {
            string message = "Project Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            var cst = DateTime.UtcNow.AddHours(-6);
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@ProjectId", ProjectId);
                _srt.Add("@comment", Comment);
                _srt.Add("@createdby", userid);
                _srt.Add("@createddate", cst);
                sqlHelper.executeNonQuery("AddProjectComment", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            jobs = getJobListbyProjectId(ProjectId);
            offers = getofferListbyProjectId(ProjectId);
            return message;
        }

        public string AddNewDocument(string FileTitle, int userid, int ProjectId, string Size, string url, ref List<int> jobs, ref List<int> offers, ref int parentid)
        {
            string message = "File Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            var cst = DateTime.UtcNow.AddHours(-6);

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@createddate", cst);
                _srt.Add("@FileTitle", FileTitle);
                _srt.Add("@ProjectId", ProjectId);
                _srt.Add("@Size", Size);
                _srt.Add("@url", url);
                _srt.Add("@createdby", userid);
                parentid = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_projectfiles_add", "", _srt).ToString());
                jobs = getJobListbyProjectId(ProjectId);
                offers = getofferListbyProjectId(ProjectId);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string delete_doc(int id, int ProjectId, ref List<int> jobs, ref List<int> offers)
        {
            string message = "File Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                sqlHelper.executeNonQuery("tbl_projectfiles_remove", "", _srt);
                jobs = getJobListbyProjectId(ProjectId);
                offers = getofferListbyProjectId(ProjectId);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public List<int> getJobListbyProjectId(int projectid)
        {
            List<int> _list = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@projectid", projectid);
                var dt = sqlHelper.fillDataTable("GetJobsByProjectId", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _list.Add(Convert.ToInt32(dt.Rows[i][0].ToString()));
                }
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _list;

        }
        public List<int> getofferListbyProjectId(int projectid)
        {
            List<int> _list = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@projectid", projectid);
                var dt = sqlHelper.fillDataTable("GetofferByProjectId", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _list.Add(Convert.ToInt32(dt.Rows[i][0].ToString()));
                }
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _list;

        }
        public ProjectModal getprojectbyId(int projectId)
        {

            SqlHelper sqlHelper = new SqlHelper();
            ProjectModal _mdl = new ProjectModal();
            List<int> _skills = new List<int>();
            List<int> _tools = new List<int>();
            List<int> _screening = new List<int>();
            List<commentmodal> _cmt = new List<commentmodal>();
            List<FileModel> _files = new List<FileModel>();
            List<reminder> techreminder = new List<reminder>();
            List<reminder> dispatcherreminder = new List<reminder>();
            List<string> technicians = new List<string>();
            List<string> dispatchers = new List<string>();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@ID", projectId);
                var ds = sqlHelper.fillDataSet("Getprojectbyid", "", _srt);
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];
                var dt3 = ds.Tables[3];
                var dt4 = ds.Tables[4];
                var dt5 = ds.Tables[5];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _mdl.Id = Convert.ToInt32(dt.Rows[0]["ProjectId"].ToString());
                    _mdl.clientId = Convert.ToInt32(dt.Rows[0]["clientId"].ToString());
                    _mdl.ProjectId = dt.Rows[0]["IndexId"].ToString();
                    _mdl.clientManagerId = Convert.ToInt32(dt.Rows[0]["ClientManagerId"].ToString());
                    _mdl.description = dt.Rows[0]["Description"].ToString();
                    _mdl.specialInstruction = dt.Rows[0]["SpecialInstruction"].ToString();
                    _mdl.projectManagerId = Convert.ToInt32(dt.Rows[0]["ProjectManagerId"].ToString());
                    _mdl.email = dt.Rows[0]["ReminderEmail"].ToString();
                    _mdl.status = Convert.ToInt32(dt.Rows[0]["Status"].ToString());
                    _mdl.title = dt.Rows[0]["ProjectName"].ToString();
                    _mdl.token = new Crypto().EncryptStringAES(dt.Rows[i]["ProjectId"].ToString());
                    _mdl._client = dt.Rows[0]["clientName"].ToString();
                    _mdl._clientManager = dt.Rows[0]["ClientManager"].ToString();
                    _mdl._projectManager = dt.Rows[0]["projectManager"].ToString();
                }
                if (dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        _skills.Add(Convert.ToInt32(dt1.Rows[i][0].ToString()));
                    }
                    _mdl.skills = _skills.ToArray();
                }

                if (dt2.Rows.Count > 0)
                {
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        _tools.Add(Convert.ToInt32(dt2.Rows[i][0].ToString()));
                    }
                    _mdl.tools = _tools.ToArray();
                }

                if (dt3.Rows.Count > 0)
                {
                    for (int i = 0; i < dt3.Rows.Count; i++)
                    {
                        _screening.Add(Convert.ToInt32(dt3.Rows[i][0].ToString()));
                    }
                    _mdl.Screening = _screening.ToArray();
                }
                //tbl_project_comment.Comment,tbl_project_comment.Createddate,tbl_member.f_name+' '+tbl_member.l_name as name
                if (dt4.Rows.Count > 0)
                {
                    for (int i = 0; i < dt4.Rows.Count; i++)
                    {
                        commentmodal _ml = new commentmodal();
                        _ml.Comment = dt4.Rows[i]["Comment"].ToString();
                        _ml.createdby = dt4.Rows[i]["name"].ToString();
                        _ml.CreatedDate = dt4.Rows[i]["Createddate"].ToString();
                        _cmt.Add(_ml);
                    }

                }
                _mdl._commnts = _cmt;

                if (dt5.Rows.Count > 0)
                {
                    for (int i = 0; i < dt5.Rows.Count; i++)
                    {
                        FileModel _fmdl = new FileModel();
                        _fmdl.FileTitle = dt5.Rows[i]["FileTile"].ToString();
                        _fmdl.FileId = Convert.ToInt32(dt5.Rows[i]["id"].ToString());
                        _fmdl.Size = dt5.Rows[i]["Size"].ToString();
                        _fmdl.Url = dt5.Rows[i]["url"].ToString();
                        _fmdl.CreatedBy = dt5.Rows[i]["name"].ToString();
                        _fmdl.CreatedDate = dt5.Rows[i]["Createddate"].ToString();
                        _fmdl.FileToken = new Crypto().EncryptStringAES(dt5.Rows[i]["id"].ToString());
                        _files.Add(_fmdl);
                    }
                }
                _mdl._files = _files;
                var dt6 = ds.Tables[6];
                var dt7 = ds.Tables[7];
                if (dt6.Rows.Count > 0)
                {
                    for (int i = 0; i < dt6.Rows.Count; i++)
                    {
                        reminder techreminder1 = new reminder();
                        techreminder1.Projectid = Convert.ToInt32(dt6.Rows[i]["Projectid"].ToString());
                        techreminder1.token = new Crypto().EncryptStringAES(dt6.Rows[i]["id"].ToString());
                        techreminder1._reminderId = Convert.ToInt32(dt6.Rows[i]["id"].ToString());
                        techreminder1.repeatid = Convert.ToInt32(dt6.Rows[i]["repeatid"].ToString());
                        techreminder1.reapetdays = Convert.ToInt32(dt6.Rows[i]["reapetdays"].ToString());
                        techreminder1.repeattype = Convert.ToInt32(dt6.Rows[i]["ReminderType"].ToString());
                        techreminder1.Attachment = dt6.Rows[i]["Attachment"].ToString().StringToArray(",");
                        techreminder1.info = dt6.Rows[i]["info"].ToString().StringToArray(",");
                        techreminder1.type = dt6.Rows[i]["type"].ToString().StringToArray(",");
                        techreminder1.Confirmation = Convert.ToInt32(dt6.Rows[i]["Confirmation"].ToString());
                        techreminder.Add(techreminder1);
                    }
                }
                _mdl.techreminder = techreminder;
                if (dt7.Rows.Count > 0)
                {
                    for (int i = 0; i < dt7.Rows.Count; i++)
                    {
                        reminder dispatcherreminder1 = new reminder();
                        dispatcherreminder1.Projectid = Convert.ToInt32(dt7.Rows[i]["Projectid"].ToString());
                        dispatcherreminder1.token = new Crypto().EncryptStringAES(dt7.Rows[i]["id"].ToString());
                        dispatcherreminder1._reminderId = Convert.ToInt32(dt7.Rows[i]["id"].ToString());
                        dispatcherreminder1.repeatid = Convert.ToInt32(dt7.Rows[i]["repeatid"].ToString());
                        dispatcherreminder1.reapetdays = Convert.ToInt32(dt7.Rows[i]["reapetdays"].ToString());
                        dispatcherreminder1.repeattype = Convert.ToInt32(dt7.Rows[i]["ReminderType"].ToString());
                        dispatcherreminder1.Attachment = dt7.Rows[i]["Attachment"].ToString().StringToArray(",");
                        dispatcherreminder1.info = dt7.Rows[i]["info"].ToString().StringToArray(",");
                        dispatcherreminder1.type = dt7.Rows[i]["type"].ToString().StringToArray(",");
                        dispatcherreminder1.Confirmation = Convert.ToInt32(dt7.Rows[i]["Confirmation"].ToString());
                        dispatcherreminder.Add(dispatcherreminder1);
                    }
                }
                _mdl.dispatcherreminder = dispatcherreminder;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                sqlHelper.Dispose();
            }
            _mdl.jobOffer = getJobofferprojectId(projectId, ref technicians, ref dispatchers);
            _mdl.technicians = technicians;
            _mdl.dispatchers = dispatchers;
            return _mdl;
        }


        public remindersetting getreminders(int _workspaceId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            remindersetting _mdl = new remindersetting();
            List<reminder> techreminder = new List<reminder>();
            List<reminder> dispatcherreminder = new List<reminder>();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@WorkspaceId", _workspaceId);
                var ds = sqlHelper.fillDataSet("GetRemindersetting", "", _srt);
                var dt6 = ds.Tables[0];
                var dt7 = ds.Tables[1];
                if (dt6.Rows.Count > 0)
                {
                    for (int i = 0; i < dt6.Rows.Count; i++)
                    {
                        reminder techreminder1 = new reminder();
                        techreminder1.Projectid = Convert.ToInt32(dt6.Rows[i]["Projectid"].ToString());
                        techreminder1.token = new Crypto().EncryptStringAES(dt6.Rows[i]["id"].ToString());
                        techreminder1._reminderId = Convert.ToInt32(dt6.Rows[i]["id"].ToString());
                        techreminder1.repeatid = Convert.ToInt32(dt6.Rows[i]["repeatid"].ToString());
                        techreminder1.reapetdays = Convert.ToInt32(dt6.Rows[i]["reapetdays"].ToString());
                        techreminder1.repeattype = Convert.ToInt32(dt6.Rows[i]["ReminderType"].ToString());
                        techreminder1.Attachment = dt6.Rows[i]["Attachment"].ToString().StringToArray(",");
                        techreminder1.info = dt6.Rows[i]["info"].ToString().StringToArray(",");
                        techreminder1.type = dt6.Rows[i]["type"].ToString().StringToArray(",");
                        techreminder1.Confirmation = Convert.ToInt32(dt6.Rows[i]["Confirmation"].ToString());
                        techreminder.Add(techreminder1);
                    }

                }
                _mdl.techReminder = techreminder;
                if (dt7.Rows.Count > 0)
                {
                    for (int i = 0; i < dt7.Rows.Count; i++)
                    {
                        reminder dispatcherreminder1 = new reminder();
                        dispatcherreminder1.Projectid = Convert.ToInt32(dt7.Rows[i]["Projectid"].ToString());
                        dispatcherreminder1.token = new Crypto().EncryptStringAES(dt7.Rows[i]["id"].ToString());
                        dispatcherreminder1._reminderId = Convert.ToInt32(dt7.Rows[i]["id"].ToString());
                        dispatcherreminder1.repeatid = Convert.ToInt32(dt7.Rows[i]["repeatid"].ToString());
                        dispatcherreminder1.reapetdays = Convert.ToInt32(dt7.Rows[i]["reapetdays"].ToString());
                        dispatcherreminder1.repeattype = Convert.ToInt32(dt7.Rows[i]["ReminderType"].ToString());
                        dispatcherreminder1.Attachment = dt7.Rows[i]["Attachment"].ToString().StringToArray(",");
                        dispatcherreminder1.info = dt7.Rows[i]["info"].ToString().StringToArray(",");
                        dispatcherreminder1.type = dt7.Rows[i]["type"].ToString().StringToArray(",");
                        dispatcherreminder1.Confirmation = Convert.ToInt32(dt7.Rows[i]["Confirmation"].ToString());
                        dispatcherreminder.Add(dispatcherreminder1);
                    }

                }
                _mdl.dispatcherReminder = dispatcherreminder;
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _mdl;
        }

        public string addNewReminder(int usertype, reminder item, int projectId, int workspaceid, int isprojectlevel, ref int reminderId)
        {
            string message = "Reminder Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string Attachment = (item.Attachment != null && item.Attachment.Count() > 0) ? item.Attachment.ToDelimitedString() : "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@usertype", usertype);
                _srt.Add("@remindertype", item.repeattype);
                _srt.Add("@projectId", projectId);
                _srt.Add("@repeatid", item.repeatid);
                _srt.Add("@reapetdays", item.reapetdays);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@isprojectlevel", isprojectlevel);
                _srt.Add("@Attachment", Attachment);
                _srt.Add("@info", item.info.ToDelimitedString());
                _srt.Add("@type", item.type.ToDelimitedString());
                _srt.Add("@Confirmation", item.Confirmation);
                reminderId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("AddReminderfromsetting", "", _srt).ToString());
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string addNewReminder(int usertype, reminder item, int workspaceid, int isprojectlevel, ref int reminderId)
        {
            string Attachment = (item.Attachment != null && item.Attachment.Count() > 0) ? item.Attachment.ToDelimitedString() : "";
            string message = "Reminder Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@usertype", usertype);
                _srt.Add("@remindertype", item.repeattype);
                _srt.Add("@projectId", 0);
                _srt.Add("@repeatid", item.repeatid);
                _srt.Add("@reapetdays", item.reapetdays);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@isprojectlevel", isprojectlevel);
                _srt.Add("@Attachment", Attachment);
                _srt.Add("@info", item.info.ToDelimitedString());
                _srt.Add("@type", item.type.ToDelimitedString());
                _srt.Add("@Confirmation", item.Confirmation);
                reminderId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("AddReminderfromsetting", "", _srt).ToString());
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }


        public string AddNewReminderWithJobId(int usertype, reminder item, int workspaceid, int isprojectlevel, int _jobId, ref int reminderId)
        {
            string Attachment = (item.Attachment != null && item.Attachment.Count() > 0) ? item.Attachment.ToDelimitedString() : "";
            string message = "Reminder Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@usertype", usertype);
                _srt.Add("@remindertype", item.repeattype);
                _srt.Add("@projectId", 0);
                _srt.Add("@repeatid", item.repeatid);
                _srt.Add("@reapetdays", item.reapetdays);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@isprojectlevel", isprojectlevel);
                _srt.Add("@Attachment", Attachment);
                _srt.Add("@info", item.info.ToDelimitedString());
                _srt.Add("@type", item.type.ToDelimitedString());
                _srt.Add("@Confirmation", item.Confirmation);
                _srt.Add("@jobId", _jobId);
                reminderId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("AddNewReminderWithJobId", "", _srt).ToString());
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string deleteReminder(int reminderid)
        {
            string message = "Reminder deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@reminderid", reminderid);
                sqlHelper.executeNonQuery("deleteeReminder", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }


        public string DeactivateReminder(int _reminderId)
        {
            string message = "Reminder deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@reminderid", _reminderId);
                sqlHelper.executeNonQuery("DeactivateReminder", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }


        public string updateReminder(int usertype, reminder item, int projectId, int reminderid)
        {
            string Attachment = (item.Attachment != null && item.Attachment.Count() > 0) ? item.Attachment.ToDelimitedString() : "";

            string message = "Reminder updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@usertype", usertype);
                _srt.Add("@remindertype", item.repeattype);
                _srt.Add("@projectId", projectId);
                _srt.Add("@repeatid", item.repeatid);
                _srt.Add("@reapetdays", item.reapetdays);
                _srt.Add("@Attachment", Attachment);
                _srt.Add("@info", item.info.ToDelimitedString());
                _srt.Add("@type", item.type.ToDelimitedString());
                _srt.Add("@reminderid", reminderid);
                _srt.Add("@Confirmation", item.Confirmation);
                sqlHelper.executeNonQuery("updateReminder", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public List<Projectjobitems> getJobofferprojectId(int projectId, ref List<string> techs, ref List<string> dispatchers)
        {
            Crypto _crypt = new Crypto();
            SqlHelper sqlHelper = new SqlHelper();
            List<Projectjobitems> _mdl = new List<Projectjobitems>();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@ID", projectId);
                var dt = sqlHelper.fillDataTable("GetjobsofferByProjectid", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Projectjobitems _items = new Projectjobitems();
                    _items.date = dt.Rows[i]["startdate"].ToString();
                    _items.dispatcher = dt.Rows[i]["dispatcher"].ToString().Length > 20 ? dt.Rows[i]["dispatcher"].ToString().Substring(0, 20) : dt.Rows[i]["dispatcher"].ToString();
                    _items.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _items.JobId = dt.Rows[i]["JobId"].ToString().Count() > 20 ? dt.Rows[i]["JobId"].ToString().Substring(0, 20) : dt.Rows[i]["JobId"].ToString();
                    _items.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _items.Location = dt.Rows[i]["cityname"].ToString() + ", " + dt.Rows[i]["statename"].ToString();
                    _items.tech = dt.Rows[i]["techname"].ToString().Length > 20 ? dt.Rows[i]["techname"].ToString().Substring(0, 20) : dt.Rows[i]["techname"].ToString();
                    if (!string.IsNullOrEmpty(_items.tech) && !techs.Contains(_items.tech))
                    {
                        techs.Add(_items.tech);
                    }
                    if (!string.IsNullOrEmpty(_items.dispatcher) && !dispatchers.Contains(_items.dispatcher))
                    {
                        dispatchers.Add(_items.dispatcher);
                    }

                    _items.title = dt.Rows[i]["Ttile"].ToString().Length > 20 ? dt.Rows[i]["Ttile"].ToString().Substring(0, 20) : dt.Rows[i]["Ttile"].ToString();
                    _items.Type = dt.Rows[i]["type"].ToString();
                    _mdl.Add(_items);
                }
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();

            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _mdl;
        }
    }
}
