﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{
    public class ContractorServices
    {
        public string AddNewContractor(ContractorModal_Item Contractor, int userid, int workspaceid)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@Clientname", Contractor.name);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@primarycontact", Contractor.primarycontactname);
                _srt.Add("@rate", string.IsNullOrEmpty(Contractor.rate) ? "$0" : Contractor.rate);
                _srt.Add("@Address", Contractor.MapValidateAddress);
                // _srt.Add("@MapValidateAddress", client.MapValidateAddress);
                // _srt.Add("@cityId", client.city_id);
                //_srt.Add("@stateId", client.state_id);
                _srt.Add("@website", Contractor.website);
                _srt.Add("@suite", Contractor.suite);
                _srt.Add("@Email", Contractor.email);
                _srt.Add("@Phone", Contractor.phone);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("AddNewContractor", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string add_new_companywithmember(ContractorModal_Item Contractor, int userid)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@company", Contractor.name);
                _srt.Add("@primaryname", Contractor.primarycontactname);
                _srt.Add("@hourlyrate", string.IsNullOrEmpty(Contractor.rate) ? "$0" : Contractor.rate);
                _srt.Add("@address", Contractor.address);
                _srt.Add("@cityid", Contractor.city_id);
                _srt.Add("@stateid", Contractor.state_id);
                _srt.Add("@website", Contractor.website);
                _srt.Add("@zip", Contractor.zipcode);
                _srt.Add("@email", Contractor.email);
                _srt.Add("@phone", Contractor.phone);
                _srt.Add("@userId", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_company_addnew", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string DeleteContractor(int ContractorId)
        {
            string message = "Contractor deleted successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@ContractorId", ContractorId);
                sqlHelper.executeNonQuery("DeleteContractor", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }
        public string delete_member(int member)
        {
            string message = "User deactivate successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@memberid", member);
                sqlHelper.executeNonQuery("tbl_member_delete_company", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }
        public string updatnotification(int memberid)
        {
            string message = "Member deactivate successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@memberid", memberid);
                sqlHelper.executeNonQuery("tbl_genralmessage_update", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }

        public string updatRemindernotification(int notification, int c)
        {
            string message = "Confirmation done successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@notification", notification);
                _srt.Add("@c", c);
                sqlHelper.executeNonQuery("UpdateNotification", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }

        public ContractorModal_Item GetAllActiveContractorById(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            ContractorModal_Item _item = new ContractorModal_Item();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@ContractorById", id);
                var dt = sqlHelper.fillDataTable("GetAllActiveContractorById", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // clientmodal_item _item = new clientmodal_item();
                    _item.website = dt.Rows[i]["website"].ToString();
                    _item.name = dt.Rows[i]["Contractorname"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.suite = dt.Rows[i]["SuiteNumber"].ToString();
                    _item.MapValidateAddress = dt.Rows[i]["Address"].ToString();
                    _item.Contractorid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    // _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            //_mdl.clients = _model;
            return _item;
        }
        public string UpdateContractor(ContractorModal_Item Contractor)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@Contractorname", Contractor.name);
                _srt.Add("@website", Contractor.website);
                _srt.Add("@primary", Contractor.primarycontactname);
                _srt.Add("@rate", string.IsNullOrEmpty(Contractor.rate) ? "$0" : Contractor.rate);
                _srt.Add("@Address", Contractor.MapValidateAddress);
                _srt.Add("@Suite", Contractor.suite);
                _srt.Add("@Email", Contractor.email);
                _srt.Add("@Phone", Contractor.phone);
                _srt.Add("@Contractorid", Contractor.Contractorid);
                message = sqlHelper.executeNonQueryWMessage("UpdateContractor", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public List<ContractorModal_Item> GetAllActiveContractorByPaging(ref int total, int startindex = 0, int endindex = 25, string search = "", int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            clientmodal _mdl = new clientmodal();
            List<ContractorModal_Item> _model = new List<ContractorModal_Item>();
            Crypto _crypt = new Crypto();
            SortedList _srtlist = new SortedList();
            _srtlist.Add("@startindex", startindex);
            _srtlist.Add("@endIndex", endindex);
            _srtlist.Add("@workspaceid", workspaceid);
            if (!string.IsNullOrEmpty(search))
            {
                _srtlist.Add("@search", search);
            }
            try
            {
                var dt = sqlHelper.fillDataTable("GetAllActiveContractorByPaging", "", _srtlist);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ContractorModal_Item _item = new ContractorModal_Item();
                    _item.name = dt.Rows[i]["ContractorName"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.Contractorid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl.clients = _model;
            return _model;
        }
        public string getalterntivephone(int clientid, ref string companyname)
        {
            SqlHelper sqlHelper = new SqlHelper();
            string _model = "";
            SortedList _srt = new SortedList();
            _srt.Add("@id", clientid);
            companyname = "";
            Crypto _crypt = new Crypto();
            try
            {
                var dt = sqlHelper.fillDataTable("getalternativebyclientid", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    _model = dt.Rows[0]["Phone"].ToString();
                    companyname = dt.Rows[0]["company"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _model;

        }
    }
}
