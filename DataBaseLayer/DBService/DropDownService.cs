﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{
    public class selectservices
    {
        public List<Select2Model> get_all_satets(int startindex, int endindex, string searchterm, int? stateid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<Select2Model> select2Models = new List<Select2Model>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@sIndex", startindex);
                sortedLists.Add("@eIndex", endindex);
                if (!string.IsNullOrEmpty(searchterm))
                {
                    sortedLists.Add("@name", searchterm);
                }
                if (stateid.HasValue && stateid.Value != 0)
                {
                    sortedLists.Add("@id", stateid.Value);
                }
                DataTable dataTable = sqlHelper.fillDataTable("tbl_state_getallactivestate", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    Select2Model select2Model = new Select2Model()
                    {
                        id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString()),
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString()),
                        name = dataTable.Rows[i]["Name"].ToString()
                    };
                    select2Models.Add(select2Model);
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }
        public List<Select2Model> get_all_city_by_stateid(int startindex, int endindex, string searchterm, int? stateid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<Select2Model> select2Models = new List<Select2Model>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@sIndex", startindex);
                sortedLists.Add("@eIndex", endindex);
                if (!string.IsNullOrEmpty(searchterm))
                {
                    sortedLists.Add("@name", searchterm);
                }
                if (stateid.HasValue && stateid.Value != 0)
                {
                    sortedLists.Add("@id", stateid.Value);
                }
                DataTable dataTable = sqlHelper.fillDataTable("tbl_city_getallactivecity_by_stateid", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    Select2Model select2Model = new Select2Model()
                    {
                        id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString()),
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString()),
                        name = dataTable.Rows[i]["Name"].ToString()
                    };
                    select2Models.Add(select2Model);
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public List<Select2Model> get_all_city_by_cityid(int startindex, int endindex, string searchterm, int? stateid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<Select2Model> select2Models = new List<Select2Model>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@sIndex", startindex);
                sortedLists.Add("@eIndex", endindex);
                if (!string.IsNullOrEmpty(searchterm))
                {
                    sortedLists.Add("@name", searchterm);
                }
                if (stateid.HasValue && stateid.Value != 0)
                {
                    sortedLists.Add("@id", stateid.Value);
                }
                DataTable dataTable = sqlHelper.fillDataTable("tbl_city_getallactivecity_by_cityid", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    Select2Model select2Model = new Select2Model()
                    {
                        id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString()),
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString()),
                        name = dataTable.Rows[i]["Name"].ToString()
                    };
                    select2Models.Add(select2Model);
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public List<Select2Model> get_all_users(int startindex, int endindex, string searchterm, int? stateid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<Select2Model> select2Models = new List<Select2Model>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@sIndex", startindex);
                sortedLists.Add("@eIndex", endindex);
                if (!string.IsNullOrEmpty(searchterm))
                {
                    sortedLists.Add("@name", searchterm);
                }
                if (stateid.HasValue && stateid.Value != 0)
                {
                    sortedLists.Add("@id", stateid.Value);
                }
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallactivemember", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    Select2Model select2Model = new Select2Model()
                    {
                        id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString()),
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString()),
                        name = dataTable.Rows[i]["Name"].ToString()
                    };
                    select2Models.Add(select2Model);
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public List<Select2Model> get_all_jobs(int startindex, int endindex, string searchterm, int? stateid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<Select2Model> select2Models = new List<Select2Model>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@sIndex", startindex);
                sortedLists.Add("@eIndex", endindex);
                if (!string.IsNullOrEmpty(searchterm))
                {
                    sortedLists.Add("@name", searchterm);
                }
                if (stateid.HasValue && stateid.Value != 0)
                {
                    sortedLists.Add("@id", stateid.Value);
                }
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallactivejob", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    Select2Model select2Model = new Select2Model()
                    {
                        id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString()),
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString()),
                        name = dataTable.Rows[i]["Name"].ToString()
                    };
                    select2Models.Add(select2Model);
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }


        public List<Select2Model> get_all_clients(int startindex, int endindex, string searchterm, int? stateid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<Select2Model> select2Models = new List<Select2Model>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@sIndex", startindex);
                sortedLists.Add("@eIndex", endindex);
                if (!string.IsNullOrEmpty(searchterm))
                {
                    sortedLists.Add("@name", searchterm);
                }
                if (stateid.HasValue && stateid.Value != 0)
                {
                    sortedLists.Add("@id", stateid.Value);
                }
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallclient", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    Select2Model select2Model = new Select2Model()
                    {
                        id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString()),
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString()),
                        name = dataTable.Rows[i]["Name"].ToString()
                    };
                    select2Models.Add(select2Model);
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public List<Select2Model> get_all_prjmgr(int startindex, int endindex, string searchterm, int? stateid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<Select2Model> select2Models = new List<Select2Model>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@sIndex", startindex);
                sortedLists.Add("@eIndex", endindex);
                if (!string.IsNullOrEmpty(searchterm))
                {
                    sortedLists.Add("@name", searchterm);
                }
                if (stateid.HasValue && stateid.Value != 0)
                {
                    sortedLists.Add("@id", stateid.Value);
                }
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallprkjmgr", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    Select2Model select2Model = new Select2Model()
                    {
                        id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString()),
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString()),
                        name = dataTable.Rows[i]["Name"].ToString()
                    };
                    select2Models.Add(select2Model);
                }
            }
            catch (Exception)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

    }
}
