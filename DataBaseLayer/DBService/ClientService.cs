﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{
    public class clientservices
    {
        public string GetLatestClientId(int _workspaceId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Profilemodel _model = new Profilemodel();
            string clientId = "";
            try
            {
                SortedList _list = new SortedList();
                // _list.Add("@Workspaceid", Workspaceid);
                clientId = sqlHelper.executeNonQueryWMessage("GetLatestClientId", "", null).ToString();
                clientId = clientId.Count() < 5 ? "0" + clientId : clientId;
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return clientId;
        }

        public string GetClientRate(int _clientId)
        {
            string message = "$1";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@clientId", _clientId);
                message = sqlHelper.executeNonQueryWMessage("GetClientRate", "", _srt).ToString().Currency();
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }

        public string CreateClient(clientmodal_item _request, int userid, int workspaceid)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@Clientname", _request.name);
                _srt.Add("@primarycontact", _request.primarycontactname);
                _srt.Add("@rate", string.IsNullOrEmpty(_request.rate) ? "$0" : _request.rate);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@Address", _request.address);
                _srt.Add("@MapValidateAddress", _request.MapValidateAddress);
                _srt.Add("@Cityid", _request.city_id);
                _srt.Add("@Stateid", _request.state_id);
                _srt.Add("@zipcode", _request.zipcode);
                _srt.Add("@Suite", _request.suite);
                _srt.Add("@Email", _request.email);
                _srt.Add("@Phone", _request.phone);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("CreateClient", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }


        public string UpdateClientProfile(clientmodal_item _request)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@Clientname", _request.name);
                _srt.Add("@primary", _request.primarycontactname);
                _srt.Add("@rate", string.IsNullOrEmpty(_request.rate) ? "$0" : _request.rate);
                _srt.Add("@Address", _request.address);
                _srt.Add("@MapValidateAddress", _request.MapValidateAddress);
                _srt.Add("@Cityid", _request.city_id);
                _srt.Add("@Stateid", _request.state_id);
                _srt.Add("@Suite", _request.suite);
                _srt.Add("@zipcode", _request.zipcode);
                _srt.Add("@Email", _request.email);
                _srt.Add("@Phone", _request.phone);
                _srt.Add("@clientid", _request.clientid);
                message = sqlHelper.executeNonQueryWMessage("UpdateClientProfile", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public List<TechnicianClientsModel> GetActiveClient(int _workspaceId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            clientmodal _mdl = new clientmodal();
            List<TechnicianClientsModel> _model = new List<TechnicianClientsModel>();
            SortedList _list = new SortedList();
            _list.Add("@workspaceId", _workspaceId);
            try
            {
                var dt = sqlHelper.fillDataTable("GetActiveClient", "", _list);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TechnicianClientsModel _item = new TechnicianClientsModel();
                    _item.ClientName = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.ClientId = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            /// //_mdl.clients = _model;
            return _model;
        }

        public List<clientmodal_item> GetAllActiveClientWithPaging(ref int _total, int _startIndex = 0, int _endIndex = 25, string _search = "", int _workspaceId = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            clientmodal _mdl = new clientmodal();
            List<clientmodal_item> _model = new List<clientmodal_item>();
            Crypto _crypt = new Crypto();
            SortedList _srtlist = new SortedList();
            _srtlist.Add("@startindex", _startIndex);
            _srtlist.Add("@endIndex", _endIndex);
            _srtlist.Add("@workspaceid", _workspaceId);
            if (!string.IsNullOrEmpty(_search))
            {
                _srtlist.Add("@search", _search);
            }
            try
            {
                var dt = sqlHelper.fillDataTable("GetAllActiveClientWithPaging", "", _srtlist);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    clientmodal_item _item = new clientmodal_item();
                    _item.name = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.index_id = dt.Rows[i]["clientIndexId"].ToString();
                    _item.clientid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl.clients = _model;
            return _model;
        }

        public clientmodal_item GetClientProfileByClientId(int _clientId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            clientmodal_item _item = new clientmodal_item();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@clientid", _clientId);
                var dt = sqlHelper.fillDataTable("GetClientProfileByClientId", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // clientmodal_item _item = new clientmodal_item();
                    _item.name = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.suite = dt.Rows[i]["SuiteNumber"].ToString();
                    _item.MapValidateAddress = dt.Rows[i]["MapValidateAddress"].ToString();
                    _item.clientid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    // _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //_mdl.clients = _model;
            return _item;
        }

        public string DeleteClient(int _clientid)
        {
            string message = "Client Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@clientid", _clientid);
                sqlHelper.executeNonQuery("DeleteClient", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }
    }
}
