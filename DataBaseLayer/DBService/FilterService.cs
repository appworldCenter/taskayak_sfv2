﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace DataBaseLayer.DBService
{
    public class filterservices
    {
        public T getfilter<T>(T t, int userId, int pageId)
        {
            typeof(T).GetProperty("isActiveFilter").SetValue(t, false);
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@userId", userId);
                sortedLists.Add("@pageId", pageId);
                var dt = sqlHelper.fillDataTable("getFilterByPageId", "", sortedLists);
                if (dt.Rows.Count > 0)
                {
                    var search = dt.Rows[0]["search"].ToString();
                    t = deserlizeJson<T>(t, search);
                    typeof(T).GetProperty("isActiveFilter").SetValue(t, true);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return t;
        }

        public void delete_filter(int userid, int pageId)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@pageId", pageId);
                _srt.Add("@userid", userid);
                sqlHelper.executeNonQuery("remove_filte", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

        }

        public void save_filter<T>(T t, int userid, int pageId)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@pageId", pageId);
                _srt.Add("@userid", userid);
                _srt.Add("@search", serlizeJson<T>(t));
                sqlHelper.executeNonQuery("Addfilter", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

        }

        //---------------------------------------
        public T deserlizeJson<T>(T t, string json)
        {
            t = JsonConvert.DeserializeObject<T>(json);
            return t;
        }

        public string serlizeJson<T>(T t)
        {
            string st = "";
            st = JsonConvert.SerializeObject(t);
            return st;
        }
    }
}
