﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace DataBaseLayer.DBService
{
    public class templateservices
    {
        public List<templateitem> get_all_active_template_bypaging(ref int total, int startindex = 0, int endindex = 25, string search = "", int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            clientmodal _mdl = new clientmodal();
            List<templateitem> _model = new List<templateitem>();
            Crypto _crypt = new Crypto();
            SortedList _srtlist = new SortedList();
            _srtlist.Add("@startindex", startindex);
            _srtlist.Add("@endIndex", endindex);
            _srtlist.Add("@workspaceid", workspaceid);
            if (!string.IsNullOrEmpty(search))
            {
                _srtlist.Add("@search", search);
            }
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_template_by_paging", "", _srtlist);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    templateitem _item = new templateitem();
                    _item.email = dt.Rows[i]["email"].ToString();
                    _item.sms = dt.Rows[i]["sms"].ToString();
                    _item.isdefault = Convert.ToBoolean(dt.Rows[i]["isdefault"].ToString());
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.title = dt.Rows[i]["title"].ToString();
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl.clients = _model;
            return _model;
        }

        public templateModal get_all_active_template(int? id, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            templateModal _mdl = new templateModal();
            List<templateitem> _model = new List<templateitem>();
            try
            {
                SortedList _srt = new SortedList();
                if (id.HasValue)
                {
                    _srt.Add("@id", id.Value);
                }
                _srt.Add("@workspaceid", workspaceid);
                var ds = sqlHelper.fillDataSet("tbl_template_getalltemplate", "", _srt);
                var dt = ds.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    templateitem _item = new templateitem();
                    _item.email = dt.Rows[i]["email"].ToString();
                    _item.sms = dt.Rows[i]["sms"].ToString();
                    _item.isdefault = Convert.ToBoolean(dt.Rows[i]["isdefault"].ToString());
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.title = dt.Rows[i]["title"].ToString();
                    _model.Add(_item);
                }
                dt.Dispose();
                ds.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._template = _model;
            return _mdl;
        }

        public string add_new_template(templateitem notice, int userid, int workspaceid)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@title", notice.title);
                _srt.Add("@sms", notice.sms);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@email", notice.email);
                _srt.Add("@default", notice.isdefault);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_template_addnew", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }


        public string update_template(templateitem template, int userid)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@title", template.title);
                _srt.Add("@sms", template.sms);
                _srt.Add("@email", template.email);
                _srt.Add("@default", template.isdefault);
                _srt.Add("@id", template.id);
                message = sqlHelper.executeNonQueryWMessage("tbl_template_update", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public templateitem get_template(int id)
        {
            templateitem _item = new templateitem();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                int i = 0;
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                var dt = sqlHelper.fillDataTable("tbl_template_getalltemplate", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    _item.email = dt.Rows[i]["email"].ToString();
                    _item.sms = dt.Rows[i]["sms"].ToString();
                    _item.isdefault = Convert.ToBoolean(dt.Rows[i]["isdefault"].ToString());
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.title = dt.Rows[i]["title"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return _item;
        }

        public string update_default(int id, bool status)
        {
            string message = "Template Updated Succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                _srt.Add("@status", status);
                sqlHelper.executeNonQuery("tbl_template_updatedefault", "", _srt);
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string getdomainname(int workspaceid)
        {
            string message = "convo";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@workspaceid", workspaceid);
                message = sqlHelper.executeNonQueryWMessage("Getdomainname", "", _srt).ToString();
            }
            catch (Exception)
            {
                //  message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }




        public string delete_template(int id)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                message = sqlHelper.executeNonQueryWMessage("tbl_template_removetemplate", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
    }
}
