﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using HubSpot.NET.Core;
using HubSpot.NET.Api.Contact.Dto;

namespace DataBaseLayer.DBService
{
    public class hubspot
    {
        private HubSpotApi api = new HubSpotApi("feb39c09-2517-417a-8e48-7c9426cb420a");
        public string CreateHubSpotCompany(HubSpotCOmpanyModel model)
        {
            try
            {
                var company = api.Company.Create(model);
                var name = model.Primarycontact.Split(' ');
                var contactmodel = new ContactHubSpotModel
                {
                    FirstName = name[0],
                    LastName = name.Length > 1 ? name[1] : "",
                    AssociatedCompanyId = company.Id,
                    Email = model.Emailaddress,
                    Phone = model.Phonenumber,
                };
                return CreateHubSpotContact(contactmodel);
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }

        }

        public string CreateHubSpotContact(ContactHubSpotModel model)
        {
            try
            {
                var contact = api.Contact.CreateOrUpdate(model);
                return contact.Id.ToString();
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
        }


    }
}
