﻿using DataBaseLayer.Helper;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DataBaseLayer.DBService
{
    public class toolservices
    {
        public List<tooltype_item> get_all_active_types(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _srt = new SortedList();
            List<tooltype_item> _mdl = new List<tooltype_item>();
            try
            {
                _srt.Add("@workspaceid", workspaceid);
                var dt = sqlHelper.fillDataTable("tbl_tooltype_getalltype_v2", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tooltype_item _item = new tooltype_item();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.typeid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.mainorder = Convert.ToInt32(dt.Rows[i]["mainorder"].ToString());
                    _mdl.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //  _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._dept = _model;
            return _mdl;
        }

        public List<tools_item> get_all_active_tools(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<tools_item> _mdl = new List<tools_item>();
            SortedList _srt = new SortedList();
            _srt.Add("@workspaceid", workspaceid);
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_tool_getalltools", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tools_item _item = new tools_item();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.toolid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.typeid = Convert.ToInt32(dt.Rows[i]["TypeId"].ToString());
                    _item.typename = dt.Rows[i]["Type"].ToString();
                    _mdl.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //  _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._dept = _model;
            return _mdl;
        }

        public string add_new_type(string name, int userid, int mainorder, int workspaceid)
        {
            string message = "Tool Type Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptName", name);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@mainorder", mainorder);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_tbltooltype_addtype", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string add_new_tool(string name, int typeid, int userid, int workspaceid)
        {
            string message = "Tool Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptName", name);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@typeid", typeid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_tbltool_addtool", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string add_new_toolbytypename(string name, string typeid, int userid)
        {
            string message = "Tool added successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptName", name);
                _srt.Add("@typename", typeid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_tbltool_addtoolbyuser", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update_tool(string name, int typeid, int toolid)
        {
            string message = "Tool Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@name", name);
                _srt.Add("@typeid", typeid);
                _srt.Add("@toolid", toolid);
                message = sqlHelper.executeNonQueryWMessage("tbl_tbltool_updatetool", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_type(string deptname, int deptid, int userid, int mainorder)
        {
            string message = "Tool Type Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptName", deptname);
                _srt.Add("@createdby", userid);
                _srt.Add("@deptid", deptid);
                _srt.Add("@mainorder", mainorder);
                message = sqlHelper.executeNonQueryWMessage("tbl_tooltype_updatetype", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string delete_type(int deptid, int userid)
        {
            string message = "Tools type Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptid", deptid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_tooltype_removetype", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public int getmaxorder(int workspaceid)
        {
            int order = 1;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@workspaceid", workspaceid);
                order = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_toolytype_getmaxdept", "", _srt).ToString());
            }
            catch (Exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return order;
        }
        public string delete_tools(int deptid, int userid)
        {
            string message = "Tools Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptid", deptid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_tools_removetools", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
    }
}
