﻿using HubSpot.NET.Api.Company.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HubSpot.NET;
using HubSpot.NET.Core;
using System.Runtime.Serialization;
namespace Model
{
    public class hubSpotModel
    {

    }

    public class HubSpotCOmpanyModel : CompanyHubSpotModel
    {
        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "phone")]
        public string Phonenumber { get; set; }
        [DataMember(Name = "message")]
        public string message { get; set; }
        //
        [DataMember(Name = "email_address")]
        public string Emailaddress { get; set; }

        [DataMember(Name = "primary_contact")]
        public string Primarycontact { get; set; }
        [DataMember(Name = "state")]
        public string state { get; set; }
        //

    }
}