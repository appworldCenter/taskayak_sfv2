﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace Model
{
    public class clientmodal : ErrorModel
    {
        public List<clientmodal_item> clients { get; set; }
       // public List<prjmngr_item> _prjlist { get; set; }
    }

    public class clientviewmodal : ErrorModel
    {
        public string cl_token { get; set; }
        public int clientid { get; set; }
        public clientmodal_item clients { get; set; }
        public List<prjmngr_item> _prjlist { get; set; }
    }

    public class clientmodal_item : ErrorModel
    {
        [Required(ErrorMessage = "Client name is required.", AllowEmptyStrings = false)]
        public string name { get; set; }
        public string index_id { get; set; }
        public string primarycontactname { get; set; }
        public string rate { get; set; }
        [Required(ErrorMessage = "Address is required.", AllowEmptyStrings = false)]
        public string address { get; set; }
        public int city_id { get; set; }
        public int clientid { get; set; }
        
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "State is required.")]
        public int state_id { get; set; }
        [Required(ErrorMessage = "City is required.", AllowEmptyStrings = false)]
        public string city { get; set; }
        public string state { get; set; }
        public string suite { get; set; }
        [Required(ErrorMessage = "Zipcode is required.", AllowEmptyStrings = false)]
        public string zipcode { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string token { get; set; }
        public string HubSpotId { get; set; }
        [Required(ErrorMessage = "Address is required.", AllowEmptyStrings = false)]
        public string MapValidateAddress { get; set; }
    }

    //--------------------------------------------------------Company Modal------------------------------------------
    //public class companyviewmodal : ErrorModel
    //{
    //    public string cl_token { get; set; }
    //    public int clientid { get; set; }
    //    public companymodal_item clients { get; set; }
    //    public List<companyprjmngr_item> _prjlist { get; set; }
    //}

    //public class compnymodal : ErrorModel
    //{
    //    public List<companymodal_item> clients { get; set; }
    //    // public List<prjmngr_item> _prjlist { get; set; }
    //}
    //public class companymodal_item : ErrorModel
    //{
    //    [Required(ErrorMessage = "Contractor name is required.", AllowEmptyStrings = false)]
    //    public string name { get; set; }
    //    public string website { get; set; }
    //    public string primarycontactname { get; set; }
    //    public string rate { get; set; }
    //    [Required(ErrorMessage = "Address is required.", AllowEmptyStrings = false)]
    //    public string address { get; set; }
    //    public int city_id { get; set; }
    //    public int clientid { get; set; }
    //    [RegularExpression("([1-9][0-9]*)", ErrorMessage = "State is required.")]
    //    public int state_id { get; set; }
    //    [Required(ErrorMessage = "City is required.", AllowEmptyStrings = false)]
    //    public string city { get; set; }
    //    public string state { get; set; }
    //    public string suite { get; set; }
    //    [Required(ErrorMessage = "Zipcode is required.", AllowEmptyStrings = false)]
    //    public string zipcode { get; set; }
    //    public string phone { get; set; }
    //    public string email { get; set; }
    //    public string token { get; set; }
    //    [Required(ErrorMessage = "Address is required.", AllowEmptyStrings = false)]
    //    public string MapValidateAddress { get; set; }
    //}


    //--------------------------------------------------------Contractor Modal------------------------------------------
    public class ContractorViewModal : ErrorModel
    {
        public string cl_token { get; set; }
        public int Contractorid { get; set; }
        public ContractorModal_Item Contractors { get; set; }
        public List<ContractorProjectManager_Item> _prjlist { get; set; }
    }

    public class ContractorModal : ErrorModel
    {
        public List<ContractorModal_Item> Contractors { get; set; }
        // public List<prjmngr_item> _prjlist { get; set; }
    }
    public class ContractorModal_Item : ErrorModel
    {
        [Required(ErrorMessage = "Contractor name is required.", AllowEmptyStrings = false)]
        public string name { get; set; }
        public string website { get; set; }
        public string primarycontactname { get; set; }
        public string rate { get; set; }
        [Required(ErrorMessage = "Address is required.", AllowEmptyStrings = false)]
        public string address { get; set; }
        public int city_id { get; set; }
        public int Contractorid { get; set; }
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "State is required.")]
        public int state_id { get; set; }
        [Required(ErrorMessage = "City is required.", AllowEmptyStrings = false)]
        public string city { get; set; }
        public string state { get; set; }
        public string suite { get; set; }
        [Required(ErrorMessage = "Zipcode is required.", AllowEmptyStrings = false)]
        public string zipcode { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string token { get; set; }
        [Required(ErrorMessage = "Address is required.", AllowEmptyStrings = false)]
        public string MapValidateAddress { get; set; }
    }
}