﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model
{
    public class attandanceadmin : ErrorModel
    {
        public List<attandanceadmins> _attandance { get; set; }
    }
    public class attandanceadmins : ErrorModel
    {
        public string token { get; set; }
        public string MemberID { get; set; }
        public string Name { get; set; }
        public int attandanceid { get; set; }
        public int paymentstatusid { get; set; }
        public string JobID { get; set; }
        public string Client { get; set; }
        public string Memberrate { get; set; }
        public string Hoursworked { get; set; }
        public string date { get; set; }
        public string Pay { get; set; }
        public string Expense { get; set; }
        public string Jobstatus { get; set; }
        public string Paymentstatus { get; set; }
    }



    public class attandanceadmin_items : ErrorModel
    {
        public string _token { get; set; }
        public string _jobtoken { get; set; }
        public string _distoken { get; set; }
        public int memberid { get; set; }
        public int dispatcherid { get; set; }
        public string dispatchername { get; set; }
        public int jobid { get; set; }
        [Required(ErrorMessage = "Please enter date", AllowEmptyStrings = false)]
        public string startdate { get; set; }
        [Required(ErrorMessage = "Please enter start time", AllowEmptyStrings = false)]
        public string starttime { get; set; }
        [Required(ErrorMessage = "Please enter end time", AllowEmptyStrings = false)]
        public string endtime { get; set; }
        public string comment { get; set; }
        public int payid { get; set; }
        public int statusid { get; set; }
        public string Comment_old { get; set; }
        public string membername { get; set; }
        public string _memberrate { get; set; }
        public List<commentmodal> _Comments { get; set; }
        public List<FileModel> _files { get; set; }

        public string createddate { get; set; }
        public string jobtitle { get; set; }
        public string jobstatus { get; set; }
        public string hourworked { get; set; }
        public string payamount { get; set; }
        public string expnse{ get; set; }
        public string clientname { get; set; }
        public int clientid { get; set; }
        public string clientrate { get; set; }
    }
}