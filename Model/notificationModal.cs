﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class notificationModal
    {
        public List<messageModal> message { get; set; }
        public int mainind { get; set; }
    }
    public enum NotificationType
    {
        Job,
        Offer,
        Pay
    }

    public class messageModal
    {
        public int msgitem { get; set; }
        public string body { get; set; }
        public string createdby { get; set; }
        public int _createdby { get; set; }
        public string createdDate { get; set; }
        public bool isread { get; set; }
        public int mainMessageid { get; set; }
    }
}