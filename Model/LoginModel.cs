﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model
{
    public class LoginModel : ErrorModel
    {
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 2)]
        [Required(ErrorMessage = "Please enter username.", AllowEmptyStrings = false)]
        public string UserName { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please enter password.", AllowEmptyStrings = false)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string Password { get; set; }
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter workspace.", AllowEmptyStrings = false)]
        public string Workspace { get; set; }
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }



    public class RegisterModel : ErrorModel
    {
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 2)]
        [Required(ErrorMessage = "Please enter First name", AllowEmptyStrings = false)]
        public string FirstName { get; set; }

        [StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 2)]
        [Required(ErrorMessage = "Please enter Last name", AllowEmptyStrings = false)]
        public string LastName { get; set; }
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter workspace", AllowEmptyStrings = false)]
        public string Workspace { get; set; }

        [Required(ErrorMessage = "Please enter email", AllowEmptyStrings = false)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please enter password", AllowEmptyStrings = false)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string Password { get; set; }
        [Compare("password", ErrorMessage = "The password and confirm password does not match")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string ConfirmPassword { get; set; }
        public bool RememberMe { get; set; }
        public int PlanId { get; set; }
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 2)]
        [Required(ErrorMessage = "Please enter company name", AllowEmptyStrings = false)]
        public string CompanyName { get; set; }
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 2)]
        [Required(ErrorMessage = "Please enter contact name", AllowEmptyStrings = false)]
        public string ContactName { get; set; }
        [Required(ErrorMessage = "Please enter phone number", AllowEmptyStrings = false)]
        public string PhoneNumber { get; set; }
        public string ReturnUrl { get; set; }
    }
}