﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model
{

    //public class rolemodal : ErrorModel
    //{
    //    public List<RoleItems> _roles { get; set; }
    //    [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
    //    public string role_name { get; set; }
    //    public int role_id { get; set; }
    //    public int statusid { get; set; }
    //}
    //public class rolecountmodal
    //{
    //    public string name { get; set; }
    //    public int y { get; set; }
    //    public string color { get; set; }
    //}
    //public class RoleItems
    //{
    //    public string Name { get; set; }
    //    public string Status { get; set; }
    //    public int RoleId { get; set; }
    //    public int StatusId { get; set; }
    //}

    public class PermissionRoleModal : ErrorModel
    {
        public List<PermissionRoleItems> _PermissionRoles { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string PermissionRole_name { get; set; }
        public int PermissionRole_id { get; set; }
        public int statusid { get; set; }
    }
    public class PermissionRoleCountModal
    {
        public string name { get; set; }
        public int y { get; set; }
        public string color { get; set; }
    }
    public class PermissionRoleItems
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public int PermissionRoleId { get; set; }
        public int StatusId { get; set; }
    }

    public class Permissions:ErrorModel
    {
        public List<PermissionsItem> _Permissions { get; set; }
        public List<int> _Userpermissions { get; set; }
        public int Ruleid { get; set; }
        public string name { get; set; }
    }
    public class PermissionsItem
    {
        public int id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string Subtype { get; set; }
    }
}