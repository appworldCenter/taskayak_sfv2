﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web; 

namespace Model
{
    public class Helper
    {
        /// <summary>
        /// Process Result to show alert on page
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_entity">Any model class</param>
        /// <param name="_message">Body text to process and show in alert</param>
        /// <returns>Model class Of type T</returns>
        public T GenerateError<T>(T _entity, string _message)
        {
            _message = _message.ToLower().Contains("succesfully") ? _message.ToLower().Replace("succesfully", "successfully") : _message;
            if (!string.IsNullOrEmpty(_message))
            {
                string FirstCharacter = _message.Substring(0, 1);
                _message = FirstCharacter.ToUpper() + _message.ToLower().Substring(1);
                if (_message.ToLower().Contains("error"))
                {
                    typeof(T).GetProperty("alertclass").SetValue(_entity, "danger");
                }
                else
                {
                    typeof(T).GetProperty("alertclass").SetValue(_entity, "success");
                }
                typeof(T).GetProperty("error_text").SetValue(_entity, _message);
            }
            return _entity;
        }

        /// <summary>
        /// Convert Enumerable list of Model class into Datatable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_entity">Enumerable list of Model class</param>
        /// <returns>DataTabel of type T</returns>
        public DataTable ConvertToDataTable<T>(List<T> _entity)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable _dataTable = new DataTable();
            for (int i = 0; i < properties.Count; i++)
            {
                PropertyDescriptor item = properties[i];
                if ((!item.PropertyType.IsGenericType ? true : item.PropertyType.GetGenericTypeDefinition() != typeof(Nullable<>)))
                {
                    _dataTable.Columns.Add(item.Name, item.PropertyType);
                }
                else
                {
                    _dataTable.Columns.Add(item.Name, item.PropertyType.GetGenericArguments()[0]);
                }
            }

            object[] _row = new object[properties.Count];
            foreach (T item in _entity)
            {
                for (int j = 0; j < (int)_row.Length; j++)
                {
                    _row[j] = properties[j].GetValue(item);
                }
                _dataTable.Rows.Add(_row);
            }
            return _dataTable;
        }

        /// <summary>
        /// Read data from file and convert it into Datatable
        /// </summary>
        /// <param name="_filePath">File file  to read data</param>
        /// <param name="_numberOfColumns">Number of columns exist in file to be convert in Datatable</param>
        /// <param name="_isFirstRowHeader">Should take first row of file as header or not (header will show in Datatable is set true else all rows taken as table)</param>
        /// <returns>DataTabel of type T</returns>
        public DataTable ConvertToDataTable(string _filePath, int _numberOfColumns, bool _isFirstRowHeader = true)
        {
            DataTable _dataTable = new DataTable();
            int _num;
            for (int i = 0; i < _numberOfColumns; i++)
            {
                _num = i + 1;
                _dataTable.Columns.Add(new DataColumn(string.Concat("Column", _num.ToString())));
            }
            string[] _strArrays = File.ReadAllLines(_filePath);
            for (int j = _isFirstRowHeader ? 1 : 0; j < (int)_strArrays.Length; j++)
            {
                string _str = _strArrays[j];
                string[] _strArrays1 = _str.Split(new char[] { ',' });
                if (!string.IsNullOrEmpty(_strArrays1[0]))
                {
                    DataRow _dataRow = _dataTable.NewRow();
                    for (int k = 0; k < _numberOfColumns; k++)
                    {
                        _dataRow[k] = _strArrays1[k];
                    }
                    _dataTable.Rows.Add(_dataRow);
                }
            }
            return _dataTable;
        }

        /// <summary>
        /// Generate random string with alpha numeric characters
        /// </summary>
        /// <param name="_charcount">Number of characters need for random string</param>
        /// <returns>Random string  with alpha numeric characters</returns>
        public string GetRandomAlphaNumeric(int? _charCount)
        {
            string _finalString = "convo";
            Random _random = new Random();
            int _count = 2;
            string _numerics = "0123456789";
            string _alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            try
            {
                if (_charCount.HasValue)
                {
                    _charCount = new int?(_charCount.GetValueOrDefault() / 2);
                    _count = Convert.ToInt32(_charCount);
                }
                char[] ChrArray = new char[_count];
                for (int i = 0; i < (int)ChrArray.Length; i++)
                {
                    ChrArray[i] = _alphabets[_random.Next(_alphabets.Length)];
                }
                char[] ChrArray1 = new char[_count];
                _random = new Random();
                for (int j = 0; j < (int)ChrArray1.Length; j++)
                {
                    ChrArray1[j] = _numerics[_random.Next(_numerics.Length)];
                }
                _finalString = string.Concat(new string(ChrArray), new string(ChrArray1));
            }
            catch (Exception)
            {

            }
            return _finalString;
        }

        /// <summary>
        /// Generate 5 digit random number
        /// </summary>
        /// <returns>5 digit random string</returns>
        public string Get5RandomDigit()
        {
            Random _randomObject = new Random();
            int _randomNumber = _randomObject.Next(111111, 999999);
            return _randomNumber.ToString("D6");
        }

        /// <summary>
        /// Generate 9 digit random number
        /// </summary>
        /// <returns>9 digit random string</returns>
        public string Get9RandomDigit()
        {
            Random _randomObject = new Random();
            int _randomNumber1 = _randomObject.Next(11111, 99999);
            int _randomNumber2 = _randomObject.Next(22222, 99999);
            return _randomNumber1.ToString("D5") + _randomNumber2.ToString("D4");
        }

        /// <summary>
        /// Create cookie  and set it to allow access on subdomain
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_encyrptedValue">Encrypted value</param>
        /// <param name="_key">Cookie key</param>
        /// <param name="_hour">Life time of a cookie in hour</param>
        public void CreateCookie<T>(T _encyrptedValue, string _key, int _hour = 5)
        {
            HttpCookie HttpCookie = new HttpCookie(_key, _encyrptedValue.ToString())
            {
                Expires = DateTime.Now.AddHours(_hour)
            };
            if (!HttpContext.Current.Request.IsLocal)
            {
                HttpCookie.Domain = Keys.MasterDomain;
            }
            HttpContext.Current.Response.Cookies.Add(HttpCookie);
        }

        /// <summary>
        /// Send email using AWS pinpoint
        /// </summary>
        /// <param name="_receiverEmail">Email address whom email is going out</param>
        /// <param name="_body">Content of email</param>
        /// <param name="_subject">Subject of email</param>
        /// <param name="_receiverName">Name whom email is going out</param>
        /// <param name="_senderName">Email sender name showing as from name in inbox</param>
        /// <param name="_senderAddress">Email sender email address showing as from in inbox</param>
        /// <returns>Email status </returns>
        /// <exception cref="SmtpException"></exception>
        ///  /// <exception cref="WebException"></exception>
        public string SendEmailWithAWS(string _receiverEmail, string _body, string _subject, string _receiverName, string _senderName = Keys.SenderName, string _senderAddress = Keys.SenderAddress)
        {
            string _result = "email sent";
            try
            {
                AlternateView _htmlBody = AlternateView.CreateAlternateViewFromString(_body, null, "text/html");
                MailMessage _message = new MailMessage();
                _message.From = new MailAddress(_senderAddress, _senderName);
                if (!string.IsNullOrEmpty(_receiverName))
                {
                    _message.To.Add(new MailAddress(_receiverEmail, _receiverName));
                }
                else
                {
                    _message.To.Add(new MailAddress(_receiverEmail));
                }
                _message.Subject = _subject;
                _message.AlternateViews.Add(_htmlBody);
                using (var _client = new System.Net.Mail.SmtpClient(Keys.SmtpEndPoint, Keys.PortNumber))
                {
                    _client.Credentials = new NetworkCredential(Keys.SmtpUserName, Keys.SmtpPassword);
                    _client.EnableSsl = true;
                    try
                    {
                        _client.Send(_message);
                    }
                    catch (Exception)
                    {
                        //     new Comman().LogWrite(Enums.LogType.Error, ex.Message.ToString(), "Utilis", "Sendmail", Enums.Priority.Critical, ex.StackTrace);
                    }
                }
            }
            catch (SmtpException smtpException)
            {
                _result = string.Concat("Error : ", smtpException.Message);
            }
            catch (WebException webException1)
            {
                WebException webException = webException1;
                using (WebResponse response = webException.Response)
                {
                    HttpWebResponse httpWebResponse = (HttpWebResponse)response;
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream))
                        {
                            _result = string.Concat("Error : ", streamReader.ReadToEnd());
                        }
                    }
                }
            }
            return _result;
        }



    }
}
