﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model
{
    public class skill : ErrorModel
    {
        public List<SkillItems> _skill { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string skill_name { get; set; }
    }

    public class SkillItems
    {
        public string Name { get; set; } 
        public int SkillId { get; set; } 
    }

    public class TechnicianClientsModel
    {
        public int ClientId { get; set; }
        public string ClientName { get; set; }
    }

    public class Tool : ErrorModel
    {
        public List<ToolItems> _tool { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string tool_name { get; set; }
    }

    public class ToolItems
    {
        public string Name { get; set; }
        public string TypeName { get; set; }
        public int ToolId { get; set; }
        public int ToolTypeId { get; set; }
    }
}