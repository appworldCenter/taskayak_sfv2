﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model
{
    public class templateModal:ErrorModel
    {
        public List<templateitem> _template { get; set; }
    }

    public class templateitem:ErrorModel
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Please enter Title", AllowEmptyStrings = false)]
        public string title { get; set; }
        public bool isdefault { get; set; }
        [Required(ErrorMessage = "Please enter Sms Message", AllowEmptyStrings = false)]
        public string sms { get; set; }
        public string email { get; set; }
    }
}