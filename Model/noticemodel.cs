﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model
{
    public class noticemodel:ErrorModel
    {
        public List<notice_items> _list { get; set; }
        public int id { get; set; }
        [Required(ErrorMessage = "Please enter title", AllowEmptyStrings = false)]
        public string titile { get; set; }
        public int statusid { get; set; }
        [Required(ErrorMessage = "Please enter description", AllowEmptyStrings = false)]
        public string descripion { get; set; }
        public string PublishedDate { get; set; }
    }

    public class notice_items
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Please enter title", AllowEmptyStrings = false)]
        public string titile { get; set; }
        public int statusid { get; set; }
        [Required(ErrorMessage = "Please enter description", AllowEmptyStrings = false)]
        public string descripion { get; set; }
        public string PublishedDate { get; set; }
    }
}