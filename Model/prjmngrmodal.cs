﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model
{
    public class prjmngrmodal:ErrorModel
    {
        public List<prjmngr_item> _prjlist { get; set; }
    }

    public class prjmngr_item : ErrorModel
    {
        public int prjmanagerid { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public int clientid { get; set; }
        public int partnerid { get; set; }
        public string prj_token { get; set; }
        public string clt_token { get; set; }
    }


    //public class companyprjmngr_item : ErrorModel
    //{
    //    public int prjmanagerid { get; set; }
    //    [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
    //    public string name { get; set; }
    //    public string email { get; set; }
    //    public string phone { get; set; }
    //    public int clientid { get; set; }
    //    public int managerid { get; set; }
    //    public string manager { get; set; }
    //    public string position { get; set; }
    //    public string prj_token { get; set; }
    //    public string clt_token { get; set; }
    //}

    public class ContractorProjectManager_Item : ErrorModel
    {
        public int prjmanagerid { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public int clientid { get; set; }
        public int managerid { get; set; }
        public string manager { get; set; }
        public string position { get; set; }
        public string prj_token { get; set; }
        public string clt_token { get; set; }
    }
}