﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model
{
    public class UserModel : ErrorModel
    {
        [Required(ErrorMessage = "Please enter first name", AllowEmptyStrings = false)]
        public string FirstName { get; set; }
        [RegularExpression("([0-9]+)", ErrorMessage = "Only numbers allowed")]
        public int DrivingDistance { get; set; }
        [Required(ErrorMessage = "Please enter user id", AllowEmptyStrings = false)]
        public string UserId { get; set; }
        [Required(ErrorMessage = "Please enter email address", AllowEmptyStrings = false)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "Please enter phone number", AllowEmptyStrings = false)]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Please enter username", AllowEmptyStrings = false)]
        public string UserName { get; set; }
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        [Required(ErrorMessage = "Please enter password", AllowEmptyStrings = false)]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "The password and confirm password does not match")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Address is required", AllowEmptyStrings = false)]
        public string Address { get; set; }
        [Required(ErrorMessage = "Zipcode is required", AllowEmptyStrings = false)]
        public string Zipcode { get; set; }
        [Required(ErrorMessage = "City is required", AllowEmptyStrings = false)]
        public string CityName { get; set; }
        public bool IsContracter { get; set; }
        public string AccountType { get; set; }
        public string Position { get; set; }
        public int CompanyId { get; set; }
        public string Token { get; set; }
        public string LastName { get; set; }
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "State is required.")]
        public int StateId { get; set; }
        public string SuiteNumber { get; set; }
        public int CityId { get; set; } 
        public string AlertnativePhoneNumber { get; set; }
        public string HourlyRate { get; set; }
        public int[] Skills { get; set; }
        public int[] Tools { get; set; }
        public int[] Clients { get; set; }
        public string Background { get; set; }
        public string DrugTested { get; set; }
        public string UserType { get; set; }
        public string Gender { get; set; }
        public int ManagerId { get; set; } 
        public string Tax { get; set; }
        public int UserTableId { get; set; }
        public int StatusId { get; set; }
        public string ImageUrl { get; set; }
        public string CityNameMailMerge { get; set; }
        public string StateNameMailMerge { get; set; }
        public string CompanyName { get; set; }
        public int RoleId { get; set; }  
        public List<PermissionRoleItems> RoleList { get; set; }
        public List<SkillItems> SkillList { get; set; }
        public List<ToolItems> ToolList { get; set; }
        public List<FileModel> DocumentList { get; set; }
        public List<TechnicianClientsModel> ClientList { get; set; }
        public List<DocumentTypeItems> DocumentTypeList { get; set; }
        [Required(ErrorMessage = "Address is required.", AllowEmptyStrings = false)]
        public string MapValidateAddress { get; set; }
    }
    public class Users : ErrorModel
    {
        public List<UserItem> UserList { get; set; }
        public List<SkillItems> SkillList { get; set; }
    }
    public class ResetModel : ErrorModel
    {
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        [Required(ErrorMessage = "Please enter password", AllowEmptyStrings = false)]
        public string Password { get; set; } 
        [Compare("password", ErrorMessage = "The password and confirm password does not match")]
        [DataType(DataType.Password)]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)] 
        public string ConfirmPassword { get; set; }
    }

    public class UserItem
    {
        public string UserId { get; set; } 
        public string Token { get; set; }
        public int StatusId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string HourlyRate { get; set; }
        public string Background { get; set; }
        public string DrugTested { get; set; }
        public string RoleName { get; set; }
        public string StatusName { get; set; }
        public string StatusColor { get; set; }
        public string Skills { get; set; }
        public int UserTableId { get; set; }
    }
    
    public class PopUpModel
    {
        public int CityId { get; set; }
        public int StateId { get; set; }
        public int ClientId { get; set; }
    }


}