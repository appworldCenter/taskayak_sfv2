﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace Model
{
    public class selectModal:ErrorModel
    {
        public List<select_item> ddl { get; set; }
    }

    public class select_item
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    public class Select2Model
    {
        public int id
        {
            get;
            set;
        }

        public string name
        {
            get;
            set;
        }

        public string poid
        {
            get;
            set;
        }

        public int total
        {
            get;
            set;
        }

    }
}