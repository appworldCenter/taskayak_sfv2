﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model
{
    public class tools : ErrorModel
    {
        public List<tooltype_item> tool_type { get; set; }
        public List<tools_item> tool_items { get; set; }
        public int typeid { get; set; }
        public int toolid { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string name { get; set; }
    }


    public class tools_item : ErrorModel
    {
        public int toolid { get; set; }
        public int typeid { get; set; }
        public string typename { get; set; }
        public string name { get; set; }
    }

    public class tooltype:ErrorModel
    {
        public List<tooltype_item> tool_type { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string name { get; set; }
        public int mainorder { get; set; }
    }

    public class tooltype_item : ErrorModel
    {
        public int typeid { get; set; }
        public string name { get; set; }
        public int mainorder { get; set; }
    }


    public class doctype : ErrorModel
    {
        public List<DocumentTypeItems> doc_type { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string name { get; set; }
        public int mainorder { get; set; }
    }

    public class DocumentTypeItems : ErrorModel
    {
        public int DocumentTypeId { get; set; }
        public string DocumentName { get; set; }
        public int ViewOrder { get; set; }
    }
}