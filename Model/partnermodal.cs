﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model
{
    public class partnermodal : ErrorModel
    {
        public List<clientmodal_item> partners { get; set; }
    }

    public class partnerviewmodal : ErrorModel
    {
        public string cl_token { get; set; }
        public int partnerid { get; set; }
        public partnermodal_item partners { get; set; }
        public List<prjmngr_item> _prjlist { get; set; }
    }

    public class partnermodal_item : ErrorModel
    {
        [Required(ErrorMessage = "Partner name is required.", AllowEmptyStrings = false)]
        public string name { get; set; }
        public string primarycontactname { get; set; }
        public string rate { get; set; }
        [Required(ErrorMessage = "Address is required.", AllowEmptyStrings = false)]
        public string address { get; set; }
        public int city_id { get; set; }
        public int partnerid { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "State is required.")]
        public int state_id { get; set; }
        [Required(ErrorMessage = "City is required.", AllowEmptyStrings = false)]
        public string city { get; set; }
        public string state { get; set; }
        [Required(ErrorMessage = "Zipcode is required.", AllowEmptyStrings = false)]
        public string zipcode { get; set; }
        public string suite { get; set; } 
        public string phone { get; set; }
        public string email { get; set; }
        public string token { get; set; }
        public string HubSpotId { get; set; }
         [Required(ErrorMessage = "Address is required.", AllowEmptyStrings = false)]
        public string MapValidateAddress { get; set; }
    }


    public class partnercompanyviewmodal : ErrorModel
    {
        public string partner_token { get; set; }
        public int partnerid { get; set; }
        public partnercompanymodal_item partners { get; set; }
        public List<ContractorProjectManager_Item> _prjlist { get; set; }
    }

    public class partnercompnymodal : ErrorModel
    {
        public List<partnercompanymodal_item> partners { get; set; }
        // public List<prjmngr_item> _prjlist { get; set; }
    }
    public class partnercompanymodal_item : ErrorModel
    {
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string name { get; set; }
        public string website { get; set; }
        public string primarycontactname { get; set; }
        public string rate { get; set; }
        public string address { get; set; }
        public int city_id { get; set; }
        public int partnerid { get; set; }
        public int state_id { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string token { get; set; }
    }
}