﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace Taskayak_v2.Utilis
{
    public class AuthorizeVerifiedloggedinAttribute : FilterAttribute, IAuthorizationFilter
    {/// <summary>
    /// Redirect to login page if try to acess page with out logged in site
    /// </summary>
    /// <param name="_filterContext"></param>
        protected void HandleUnauthorizedRequest(AuthorizationContext _filterContext)
        {
            _filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary()
            {
                { "action", "login" },
                { "controller", "account" },
                { "ReturnUrl", _filterContext.HttpContext.Request.Url.ToString() }
            });
        }
        /// <summary>
        /// Redirect user to maintance page untill admin remove Maintenance
        /// </summary>
        /// <param name="_filterContext"></param>
        protected void HandleMaintenanceRequest(AuthorizationContext _filterContext)
        {
            _filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary()
            {
                { "action", "Maintenance" },
                  { "controller", "main" },
                { "ReturnUrl", _filterContext.HttpContext.Request.Url.ToString() }
            });
        }

        /// <summary>
        /// Authorize filter calling on controller or methods
        /// </summary>
        /// <param name="_filterContext"></param>
        void System.Web.Mvc.IAuthorizationFilter.OnAuthorization(AuthorizationContext _filterContext)
        {
            bool _flag = false;
            List<int> _allowUserList = new List<int>();
            bool _redirect = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["isredirect"]);
            string _userIdList = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["userList"]);
            if (_redirect)
            {
                if (_filterContext.HttpContext.Request.Cookies["_xid"] == null)
                {
                    this.HandleUnauthorizedRequest(_filterContext);
                }
                else
                {
                    HttpCookie _cultureCookie = _filterContext.RequestContext.HttpContext.Request.Cookies["_xid"];
                    if (!string.IsNullOrEmpty(_userIdList))
                    {
                        _allowUserList = _userIdList.Split(',').Select(int.Parse).ToList();
                    }
                    int _userId = Convert.ToInt32(_cultureCookie.Value);
                    if (!_allowUserList.Contains(_userId))
                    {
                        this.HandleMaintenanceRequest(_filterContext);
                    }
                } 
            }
            else
            {
                if (_filterContext.HttpContext.Request.Cookies["_xid"] != null)
                {
                    _flag = true;
                }
                if (!_flag)
                {
                    this.HandleUnauthorizedRequest(_filterContext);
                }
            }

        }
    }
}