﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Taskayak_v2.Utilis
{
    public class CookieBuilder
    {
        /// <summary>
        /// Create a cookie
        /// </summary>
        /// <param name="_key">Name of cookie</param>
        /// <param name="_value">Value which need to save</param>
        /// <param name="_islocal">Creating cookie for local or live url</param>
        /// <param name="_year">Cookie life time in years</param>
        public void Set(string _key, string _value, bool _islocal, int _year = 1)
        {
            HttpCookie cookie = GetCookie(_islocal);
            cookie[_key] = _value;
            cookie.Expires = DateTime.Now.AddYears(_year);
            if (!_islocal)
            {
                cookie.Domain = Keys.MasterSubDomain;
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Get a cookie by key
        /// </summary>
        /// <param name="_key">Name of cookie</param>
        /// <param name="_islocal">Getting cookie for local or live url</param>
        /// <returns>Value of cookie base of passed key</returns>
        public string Get(string _key, bool _islocal)
        {
            HttpCookie cookie = GetCookie(_islocal);
            if (cookie[_key] != null)
            {
                return cookie[_key];
            }
            return "";
        }

        /// <summary>
        /// Get remember me cookie for login screen
        /// </summary>
        /// <param name="_islocal">Getting cookie for local or live url</param>
        /// <returns>Value of cookie for key "remember me"</returns>
        private HttpCookie GetCookie(bool _islocal)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["rememberme"];
            if (cookie == null)
            {
                cookie = new HttpCookie("rememberme");
                if (!_islocal)
                {
                    cookie.Domain =Keys.MasterSubDomain;
                }
                HttpContext.Current.Request.Cookies.Add(cookie);
            }
            return cookie;
        }
    }
}