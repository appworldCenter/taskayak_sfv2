﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Taskayak_v2.Utilis
{
    public class JsonpResult : JsonResult
    {
        public override void ExecuteResult(ControllerContext _context)
        {
            if (_context == null)
            {
                throw new ArgumentNullException("context");
            }
            HttpRequestBase _request = _context.HttpContext.Request;
            HttpResponseBase _response = _context.HttpContext.Response;
            string _item = _context.RouteData.Values["callback"] as string ?? _request["callback"];
            if (!string.IsNullOrEmpty(_item))
            {
                if (string.IsNullOrEmpty(base.ContentType))
                {
                    base.ContentType = "application/x-javascript";
                }
                _response.Write(string.Format("{0}(", _item));
            }
            base.ExecuteResult(_context);
            if (!string.IsNullOrEmpty(_item))
            {
                _response.Write(")");
            }
        }
    }
}